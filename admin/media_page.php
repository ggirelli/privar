<div class="wrap">
	<h2>Private Area - <?php echo PASET0c; ?></h2>
	<br/>
	<h3><?php echo PAMEDTIT0; ?></h3>

	<?php

	#Delete file
	if ( isset( $_GET['del'] ) ) {

		$file = $_GET['del'];

		if ( isset( $_POST['del'] ) ) {
			
			$path = WP_CONTENT_DIR . "/privar-archive/" . $file;

			if ( file_exists( $path ) ) {
				unlink( $path );
				echo PAMEDDEL1;
			} else {
				echo PAMEDDEL2;
			}
			echo PAMED1;
			header( "refresh: 5; url=" . $_SERVER['PHP_FILE'] . "?page=privar-media" ); 

		} else { include( dirname(dirname(__FILE__)) . "/inc/del_confirmation.inc.php" ); }

		die();
	}

	#Upload file
	if ( isset ($_FILES['privar-file'] ) ) {

		#Check for uploading errors
		if ( $_FILES["privar-file"]["error"] > 0 ) {
			echo PAMMEDUP1 . ": " . $_FILES["privar-file"]["error"] . "<br/>";
		} else {

			#Change file name if necessary
			$path = WP_CONTENT_DIR . "/privar-archive/" . $_FILES["privar-file"]["name"];
			while ( file_exists( $path ) ) {
				$_FILES["privar-file"]["name"] = "_" . $_FILES["privar-file"]["name"];
			}

			#Get file content and write it out
			$content = file_get_contents( $_FILES["privar-file"]["tmp_name"] );
			$f = fopen( $path, "w+" );
			fwrite( $f, $content );
			fclose( $f );

			#Display result to user
			echo PAMEDUP2 . " <i>" . $_FILES["privar-file"]["name"] . "</i><br/>";
			echo PAMED1;
			header( "refresh: 5; url=" . $_SERVER['PHP_FILE'] . "?page=privar-media" ); 
		}

	} else {

		#Show form
		include( dirname(dirname(__FILE__)) . "/inc/upload.form.inc.php" );

	?><h3><?php echo PAMEDTIT1; ?></h3><?php

		#Show uploaded files
		include( dirname(dirname(__FILE__)) . "/inc/media_table.inc.php" );

	}
	?>
</div>

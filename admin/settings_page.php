<div class="wrap">
	<h2>Private Area - <?php echo PASET0; ?></h2>

	<?php

	#Save Changes
	if ( isset( $_POST['alsonly'] ) ) {

		if ( $_POST['wp-log'] == true ) { update_option( 'privar-wplog', 'checked' ); }
		else { update_option( 'privar-wplog', 'unchecked' ); }
		update_option( 'privar-alsonly', $_POST['alsonly'] );
		update_option( 'privar-login-attempt', $_POST['max-attempts'] );
		update_option( 'privar-sign-email-sender-name', $_POST['name'] );
		if ( filter_var( $_POST['address'], FILTER_VALIDATE_EMAIL ) ) { update_option( 'privar-sign-email-sender-address', $_POST['address'] ); }
		if ( filter_var( $_POST['admin-address'], FILTER_VALIDATE_EMAIL ) ) { update_option( 'privar-sign-email-admin-address', $_POST['admin-address'] ); }

		if ( isset( $_POST['smtp-check'] ) ) {
			update_option( 'privar-smtp-check', '1' );

			update_option( 'privar-smtp-server', $_POST['smtp-server'] );
			update_option( 'privar-smtp-port', $_POST['smtp-port'] );
			update_option( 'privar-smtp-conn', $_POST['smtp-conn'] );
			update_option( 'privar-smtp-username', $_POST['smtp-usr'] );
			update_option( 'privar-smtp-password', $_POST['smtp-pwd'] );
		} else {
			update_option( 'privar-smtp-check', '0' );
		}

		#Display result to user
		echo "<i>" . PASET1 . "</i><br/>";
		echo PASET2;
		header( "refresh: 5; url=" . $_SERVER['PHP_FILE'] . "?page=privar" ); 
	} else {

		#Test email
		if ( isset( $_GET['test'] ) and @$_GET['test'] == 'mail' ) {

			$body = file_get_contents( dirname(dirname(__FILE__)) . '/inc/test.email.html');
			$body = eregi_replace("[\]",'',$body);

			if ( get_option( 'privar-smtp-check' ) ) {
				require_once( dirname(dirname(__FILE__)) . '/lib/PHPMailer/class.phpmailer.php' );
				echo "<div class='updated'>";
				$mail = new PHPMailer();

				$mail->ClearAllRecipients();
				$mail->ClearAddresses();
				$mail->ClearAttachments();
				$mail->CharSet = 'utf-8';

				$mail->IsSMTP();
				$mail->Host = get_option( 'privar-smtp-server' );
				$mail->SMTPDebug = 2;

				if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
					$mail->SMTPSecure = "ssl";
				} elseif ( get_option( 'privar-smtp-conn' ) == 'tsl' ) {
					$mail->SMTPSecure = "tsl";
				}
				$mail->Port = get_option( 'privar-smtp-port' );

				if ( get_option( 'privar-smtp-username' ) != "" and get_option( 'privar-smtp-password' ) != "" ) {
					$mail->SMTPAuth = true;
					$mail->Username = get_option( 'privar-smtp-username' );
					$mail->Password = get_option( 'privar-smtp-password' );
				}

				$mail->SetFrom( get_option( 'privar-sign-email-sender-address' ), get_option( 'privar-sign-email-sender-name' ) );
				$mail->Subject = 'Test';
				$mail->MsgHTML($body);

				$mail->AddAddress( get_option( 'privar-sign-email-admin-address' ) );
				$mail->AddReplyTo( get_option( 'privar-sign-email-sender-address' ) );

				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo . "</div>";
				} else {
				  echo "Message sent!</div>";
				}
			} else {
				$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
				$headers[] = 'Content-type: text/html';

				wp_mail( get_bloginfo( 'admin_email' ), ADMEMAILSUBJECT, $cont, $headers );
			}

		}

	#Show form
	?>

	<style type="text/css">

		form {
			font-size: 14px;
			line-height: 1em;
		}

		label {
			display: inline-block;
			padding: 5px 0;
		}
	</style>
	
	<h3><?php _e( 'Login settings', 'privar' ); ?></h3>
	<form action="<?php echo $_SERVER['PHP_SELF']; ?>?page=privar" method="post">
		<p style="margin-bottom: 0;"><?php echo PASETFE1; ?></p>
		<input type="radio" name="alsonly" value="n" id="no" <?php
		if ( get_option( 'privar-alsonly' ) == 'n' ) {
			echo 'checked';
		}
		?> /> <label for="no"><?php echo PASETFE5; ?></label><br/>

		<input type="radio" name="alsonly" value="a" id="also" <?php
		if ( get_option( 'privar-alsonly' ) == 'a' ) {
			echo 'checked';
		}
		?> /> <label for="also"><?php echo PASETFE2; ?></label><br/>

		<input type="radio" name="alsonly" value="o" id="only" <?php
		if ( get_option( 'privar-alsonly' ) == 'o' ) {
			echo 'checked';
		}
		?> /> <label for="only"><?php echo PASETFE3; ?></label><br/>

		<input type="radio" name="alsonly" value="b" id="both" <?php
		if ( get_option( 'privar-alsonly' ) == 'b' ) {
			echo 'checked';
		}
		?> /> <label for="both"><?php echo PASETFE4; ?></label><br/>
		<br />

		<label for="max-attempts"><?php _e( 'Number of log in attempts before captcha step', 'privar' ); ?>:</label> <input type="text" name="max-attempts" value="<?php echo get_option( 'privar-login-attempt' ); ?>" /><br />

		<hr/>
		<h3><?php _e( 'Email settings', 'privar' ); ?></h3>
		<?php echo PASETFE6 ?><br/>
		<br/>
		<?php echo PASETFE7b; ?> <input type="text" name="admin-address" value="<?php echo get_option( 'privar-sign-email-admin-address' ); ?>" /><br/>
		<?php echo PASETFE7; ?> <input type="text" name="address" value="<?php echo get_option( 'privar-sign-email-sender-address' ); ?>" /><br/>
		<?php echo PASETFE8; ?> <input type="text" name="name" value="<?php echo get_option( 'privar-sign-email-sender-name' ); ?>" /><br/>
		<hr/>
		<input type="checkbox" name="smtp-check" id="smtp-check" <?php
			if ( get_option( 'privar-smtp-check' ) == '1' ) {
				echo 'checked';
			}
		?> />&nbsp;<label for="smtp-check"><?php echo SMTPOPT; ?></label>
		<div id="smtp-opts">
			<table style="margin-left: 3em;">
				<tr>
					<td style="text-align: right;"><label for="smtp-server"><?php echo SMTPOPT1; ?></label>&nbsp;</td>
					<td><input type="text" id="smtp-server" name="smtp-server" value="<?php echo get_option( 'privar-smtp-server' ); ?>" /></td>
				</tr>
				<tr>
					<td style="text-align: right;"><label for="smtp-port"><?php echo SMTPOPT2; ?></label>&nbsp;</td>
					<td><input type="text" id="smtp-port" name="smtp-port" value="<?php echo get_option( 'privar-smtp-port' ); ?>" /></td>
				</tr>
				<tr>
					<td style="text-align: right;"><?php echo SMTPOPT5a; ?></td>
					<td>
						<input type="radio" name="smtp-conn" id="smtp-conn-1" value="no" <?php
							if ( get_option( 'privar-smtp-conn' ) == 'no' ) {
								echo 'checked ';
							}
						?> /><label for="smtp-conn-1"><?php echo SMTPOPT5b; ?></label><br />
						<input type="radio" name="smtp-conn" id="smtp-conn-2" value="ssl" <?php
							if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
								echo 'checked ';
							}
						?> /><label for="smtp-conn-2"><?php echo SMTPOPT5c; ?></label><br />
						<input type="radio" name="smtp-conn" id="smtp-conn-3" value="tls" <?php
							if ( get_option( 'privar-smtp-conn' ) == 'tls' ) {
								echo 'checked ';
							}
						?> /><label for="smtp-conn-3"><?php echo SMTPOPT5d; ?></label><br />
					</td>
				</tr>
				<tr>
					<td></td>
					<td style="padding: 0.5em 0;"><b><?php echo SMTPTXT; ?></b></td>
				</tr>
				<tr>
					<td style="text-align: right;"><label for="smtp-usr"><?php echo SMTPOPT3; ?></label>&nbsp;</td>
					<td><input type="text" id="smtp-usr" name="smtp-usr" value="<?php echo get_option( 'privar-smtp-username' ); ?>" /></td>
				</tr>
				<tr>
					<td style="text-align: right;"><label for="smtp-pwd"><?php echo SMTPOPT4; ?></label>&nbsp;</td>
					<td><input type="password" id="smtp-pwd" name="smtp-pwd" value="<?php echo get_option( 'privar-smtp-password' ); ?>" /></td>
				</tr>
			</table>
		</div><br/>
		<input type="button" class="button action" value="<?php echo PASET3b; ?>" onclick="javascript:document.location.href='?page=privar&test=mail'" /><br />
		<hr/>
		<input type="submit" class="button action" value="<?php echo PASET3; ?>" />
	</form>

	<script type="text/javascript">
	jQuery(document).ready(function($) {
		if ( $('#smtp-check').is(':checked') ) {
			$('#smtp-opts').css({'display':'block'});
		} else {
			$('#smtp-opts').css({'display':'none'});
		}

		$('#smtp-check').change(function() {
			if ( $(this).is(':checked') ) {
				$('#smtp-opts').css({'display':'block'});
			} else {
				$('#smtp-opts').css({'display':'none'});
			}
		});
	});
	</script>

	<?php

	}

	?>
</div>

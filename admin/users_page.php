<div class="wrap">
	<h2>Private Area - <?php echo PASET0b; ?></h2>

	<?php

	require_once( dirname(dirname(__FILE__)) . '/lib/users.class.php' );

	$users = new Users();

	if ( isset( $_GET['id'] ) and isset( $_GET['action'] ) ) {
		$id = $_GET['id'];

		if ( $_GET['action'] == 'act' ) {

			$data = array( 'flag_status' => 'a', 'activation_email' => '1' );
			$format = array( '%s', '%s' );
			$where = array( 'id_users' => $id);
			$where_format = array( '%d' );
			if ( $users->update( $users->table_name, $data, $where, $format, $where_format) == false ) { 

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			} else {

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			}

		} elseif ( $_GET['action'] == 'deact' ) {

			if ( isset( $_GET['else'] ) ) {

				if ( $_GET['else'] == '0' ) {
					$fs = 'w';
				} elseif ( $_GET['else'] == '1' ) {
					$fs = 'i';
				}

				$data = array( 'flag_status' => $fs, 'activation_email' => '0' );
				$format = array( '%s', '%s' );
				$where = array( 'id_users' => $id);
				$where_format = array( '%d' );
				if ( $users->update( $users->table_name, $data, $where, $format, $where_format) == false ) { 

					#Display result to user
					echo '<div id="setting-error-settings_updated" class="updated settings-error">';
					echo "<i>" . PAUSACT0 . "</i><br/>";
					echo PAUSACT1;
					echo '</div>';

					#Send confirmation email to activated user
					$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
					$headers[] = 'Content-type: text/html';

					$cont = file_get_contents( dirname(dirname(__FILE__)) . '/inc/user_activation.email.html');
					$cont = str_replace( EMAILKEYWORDTEXT, EMAILTEXT2, $cont );

					wp_mail( $users->getEmail( $id ), EMAILSUBJECT2, $cont, $headers );

				} else {
					
					#Display result to user
					echo '<div id="setting-error-settings_updated" class="updated settings-error">';
					echo "<i>" . PAUSACT0 . "</i><br/>";
					echo PAUSACT1;
					echo '</div>';

				}

			} else {

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT2 . "</i><br/>";
				echo '</div>';

			}

		} elseif ( $_GET['action'] == 'del' ) {

			$data = array( 'flag_drop' => '1' );
			$format = array( '%s' );
			$where = array( 'id_users' => $id);
			$where_format = array( '%d' );
			if ( $users->update( $users->table_name, $data, $where, $format, $where_format) == false ) { 

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			} else {
			
				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			}

		} elseif ( $_GET['action'] == 'regen' ) {

			$data = array( 'flag_drop' => '0' );
			$format = array( '%s' );
			$where = array( 'id_users' => $id);
			$where_format = array( '%d' );
			if ( $users->update( $users->table_name, $data, $where, $format, $where_format) == false ) { 

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			} else {
		
				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT0 . "</i><br/>";
				echo PAUSACT1;
				echo '</div>';

			}

		} else {

				#Display result to user
				echo '<div id="setting-error-settings_updated" class="updated settings-error">';
				echo "<i>" . PAUSACT2 . "</i><br/>";
				echo '</div>';

		}
	}

	include( dirname(dirname(__FILE__)) . '/inc/users_table.inc.php' );

	?>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		$('td.flag_status').each(function() {
			if ( $(this).text() == 'a' ) {
				$(this).text('');
				$(this).css({'background-color':'green'});
			} else if ( $(this).text() == 'i' ) {
				$(this).text('');
				$(this).css({'background-color':'orange'});
			} else if ( $(this).text() == 'w' ) {
				$(this).text('');
				$(this).css({'background-color':'red'});
			}
		});

		$('td.flag_drop').each(function() {
			if ( $(this).text() == '0' ) {
				$(this).text('N');
				$(this).parent('tr').css({'text-decoration':'none'});
			} else if ( $(this).text() == '1' ) {
				$(this).text('Y');
				$(this).parent('tr').css({'text-decoration':'line-through'});
			}
		});

		$('#setting-error-settings_updated').delay(2000).slideUp(800);
		
	});
</script>
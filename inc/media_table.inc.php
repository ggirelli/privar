<?php

require_once( dirname(dirname(__FILE__)) . '/lib/wpTable.class.php' );

function get_media_table() {
	$table = array();

	$files = scandir( WP_CONTENT_DIR . "/privar-archive/" );

	$k = 0;
	foreach ( $files as $file ) {		
		if ( !in_array( $file, array( ".", "..", ".htaccess", "download.inc.php" ) ) )  {
			$temp = new stdClass();
			$temp->file_name = $file;

			$temp->file_id = $k;
			$temp->link = '<a href="' . plugins_url( 'privar-download.php', dirname(__FILE__) ) . '?id=' . $file . '">' . __( 'Link', 'privar' ) . '</a>';
			$table[] = $temp;

			$k++;
		}
	}

	return $table;
}

$args = array(
	'table_name' => get_media_table(),
	'columns' => array(
		'file_name' => __( 'Filename', 'privar' ),
		'file_id' => __( 'ID', 'privar' ),
		'link' => __( 'Access link', 'privar' )
	),
	'hidden' => array( 'file_id' ),
	'id_column' => 'file_id',
	'single_actions' => array( __( 'Remove', 'privar' ) => array( '&del=%file_name%' ) ),
	'single_action_position' => 'file_name',
	'is_table' => true,
	'sortable' => array( 'file_name' ),
	'base_uri' => get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=privar-media',
	'class' => 'media',
	'checkbox' => false,
);

$t = new WpTable( $args );

?>

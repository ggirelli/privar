<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/wp-load.php" );
require_once( dirname(dirname(__FILE__)) . "/privar-settings.php" );
?>

<form action="<?php echo plugins_url( 'privar-recover.php', dirname(__FILE__) ) . '?code=' . $code; ?>" method="post">
	<input type="hidden" name="dt" value="<?php echo $directto; ?>" />
	<table cellspacing="0">
		<tr>
			<td><label for="<?php echo PAUSRFIELD; ?>"><?php _e( 'Username or Email', 'privar'); ?>: </label></td>
			<td><input type="text" name="<?php echo PAUSRFIELD; ?>" /></td>
		</tr>
		<tr>
			<td><label for="password"><?php echo __( 'Password: ', 'privar' ); ?></label></td>
			<td><input type="password" name="password" id="password" /></td>
		</tr>
		<tr>
			<td><label for="password2"><?php echo __( 'Confirm password: ', 'privar' ); ?></label></td>
			<td><input type="password" name="password2" id="password2" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" class='button action' value="<?php _e( 'Recover', 'privar' ); ?>" /></td>
		</tr>
	</table>
</form>
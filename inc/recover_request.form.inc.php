<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/wp-load.php" );
require_once( dirname(dirname(__FILE__)) . "/privar-settings.php" );
?>

<form action="<?php echo plugins_url( 'privar-recover.php', dirname(__FILE__) ); ?>" method="post">
	<table cellspacing="0">
		<tr>
			<td><label for="<?php echo PAUSRFIELD; ?>"><?php _e( 'Username or Email', 'privar'); ?>: </label></td>
			<td><input type="text" name="<?php echo PAUSRFIELD; ?>" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" class='button action' value="<?php _e( 'Recover', 'privar' ); ?>" /></td>
		</tr>
	</table>
</form>
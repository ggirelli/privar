<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(dirname(__FILE__))))) . "/wp-load.php" );
require_once( dirname(dirname(__FILE__)) . "/privar-settings.php" );

?>

<style type="text/css">
	td .error {
		border: 1px solid red;
		background-color: lightpink;
	}

	.error-log {
		margin: 0.5em 0;
	    padding-left: 1em;
	    border-left: 5px solid red;
	    font-size: 14px;
	    color: red;
	}
</style>

<form action="<?php echo plugins_url( 'privar-signer.php', dirname(__FILE__) ); ?>" method="post" id="privar-signin">
	<div class="error-log"></div>
	<table>
		<tr>
			<td><label for="name"><?php echo __( 'First Name: ', 'privar' );  ?></label></td>
			<td><input type="text" name="name" id="name" /></td>
		</tr>
		<tr>
			<td><label for="surname"><?php echo __( 'Last Name: ', 'privar' ); ?></label></td>
			<td><input type="text" name="surname" id="surname" /></td>
		</tr>
		<tr>
			<td><label for="email"><?php echo __( 'Email: ', 'privar' ); ?></label></td>
			<td><input type="text" name="email" id="email" /></td>
		</tr>
		<tr>
			<td><label for="username"><?php echo __( 'Username: ', 'privar' ); ?></label></td>
			<td><input type="text" name="username" id="username" /></td>
		</tr>
		<tr>
			<td><label for="password"><?php echo __( 'Password: ', 'privar' ); ?></label></td>
			<td><input type="password" name="password" id="password" /></td>
		</tr>
		<tr>
			<td><label for="password2"><?php echo __( 'Confirm password: ', 'privar' ); ?></label></td>
			<td><input type="password" name="password2" id="password2" /></td>
		</tr>
		<tr>
			<td><label for="company"><?php echo __( 'Company: ', 'privar' ); ?></label></td>
			<td><input type="text" name="company" id="company" /></td>
		</tr>
		<tr>
			<td><label for="address"><?php echo __( 'Address: ', 'privar' ); ?></label></td>
			<td><input type="text" name="address" id="address" /></td>
		</tr>
		<tr>
			<td><label for="city"><?php echo __( 'City: ', 'privar' ); ?></label></td>
			<td><input type="text" name="city" id="city" /></td>
		</tr>
		<tr>
			<td><label for="zipcode"><?php echo __( 'Zipcode: ', 'privar' ); ?></label></td>
			<td><input type="text" name="zipcode" id="zipcode" /></td>
		</tr>
		<tr>
			<td><label for="state"><?php echo __( 'State: ', 'privar' ); ?></label></td>
			<td><input type="text" name="state" maxlength="2" id="state" maxlength="2" /></td>
		</tr>
		<tr>
			<td><label for="country"><?php echo __( 'Country: ', 'privar' ); ?></label></td>
			<td>
				<select id="country" name="country" size="1">
					<option>Afghanistan</option>
					<option>land Islands</option>
					<option>Albania</option>
					<option>Algeria</option>
					<option>American Samoa</option>
					<option>Andorra</option>
					<option>Angola</option>
					<option>Anguilla</option>
					<option>Antarctica</option>
					<option>Antigua and Barbuda</option>
					<option>Argentina</option>
					<option>Armenia</option>
					<option>Aruba</option>
					<option>Australia</option>
					<option>Austria</option>
					<option>Azerbaijan</option>
					<option>Bahamas</option>
					<option>Bahrain</option>
					<option>Bangladesh</option>
					<option>Barbados</option>
					<option>Belarus</option>
					<option>Belgium</option>
					<option>Belize</option>
					<option>Benin</option>
					<option>Bermuda</option>
					<option>Bhutan</option>
					<option>Bolivia</option>
					<option>Bosnia and Herzegovina</option>
					<option>Botswana</option>
					<option>Bouvet Island</option>
					<option>Brazil</option>
					<option>British Indian Ocean territory</option>
					<option>Brunei Darussalam</option>
					<option>Bulgaria</option>
					<option>Burkina Faso</option>
					<option>Burundi</option>
					<option>Cambodia</option>
					<option>Cameroon</option>
					<option>Canada</option>
					<option>Cape Verde</option>
					<option>Cayman Islands</option>
					<option>Central African Republic</option>
					<option>Chad</option>
					<option>Chile</option>
					<option>China</option>
					<option>Christmas Island</option>
					<option>Cocos (Keeling) Islands</option>
					<option>Colombia</option>
					<option>Comoros</option>
					<option>Congo</option>
					<option>Congo, Democratic Republic</option>
					<option>Cook Islands</option>
					<option>Costa Rica</option>
					<option>Cte d'Ivoire</option>
					<option>Croatia (Hrvatska)</option>
					<option>Cuba</option>
					<option>Cyprus</option>
					<option>Czech Republic</option>
					<option>Denmark</option>
					<option>Djibouti</option>
					<option>Dominica</option>
					<option>Dominican Republic</option>
					<option>East Timor</option>
					<option>Ecuador</option>
					<option>Egypt</option>
					<option>El Salvador</option>
					<option>Equatorial Guinea</option>
					<option>Eritrea</option>
					<option>Estonia</option>
					<option>Ethiopia</option>
					<option>Falkland Islands</option>
					<option>Faroe Islands</option>
					<option>Fiji</option>
					<option>Finland</option>
					<option>France</option>
					<option>French Guiana</option>
					<option>French Polynesia</option>
					<option>French Southern Territories</option>
					<option>Gabon</option>
					<option>Gambia</option>
					<option>Georgia</option>
					<option>Germany</option>
					<option>Ghana</option>
					<option>Gibraltar</option>
					<option>Greece</option>
					<option>Greenland</option>
					<option>Grenada</option>
					<option>Guadeloupe</option>
					<option>Guam</option>
					<option>Guatemala</option>
					<option>Guinea</option>
					<option>Guinea-Bissau</option>
					<option>Guyana</option>
					<option>Haiti</option>
					<option>Heard and McDonald Islands</option>
					<option>Honduras</option>
					<option>Hong Kong</option>
					<option>Hungary</option>
					<option>Iceland</option>
					<option>India</option>
					<option>Indonesia</option>
					<option>Iran</option>
					<option>Iraq</option>
					<option>Ireland</option>
					<option>Israel</option>
					<option selected="selected">Italy</option>
					<option>Jamaica</option>
					<option>Japan</option>
					<option>Jordan</option>
					<option>Kazakhstan</option>
					<option>Kenya</option>
					<option>Kiribati</option>
					<option>Korea (north)</option>
					<option>Korea (south)</option>
					<option>Kuwait</option>
					<option>Kyrgyzstan</option>
					<option>Lao People's Democratic Republic</option>
					<option>Latvia</option>
					<option>Lebanon</option>
					<option>Lesotho</option>
					<option>Liberia</option>
					<option>Libyan Arab Jamahiriya</option>
					<option>Liechtenstein</option>
					<option>Lithuania</option>
					<option>Luxembourg</option>
					<option>Macao</option>
					<option>Macedonia, Former Yugoslav Republic Of</option>
					<option>Madagascar</option>
					<option>Malawi</option>
					<option>Malaysia</option>
					<option>Maldives</option>
					<option>Mali</option>
					<option>Malta</option>
					<option>Marshall Islands</option>
					<option>Martinique</option>
					<option>Mauritania</option>
					<option>Mauritius</option>
					<option>Mayotte</option>
					<option>Mexico</option>
					<option>Micronesia</option>
					<option>Moldova</option>
					<option>Monaco</option>
					<option>Mongolia</option>
					<option>Montenegro</option>
					<option>Montserrat</option>
					<option>Morocco</option>
					<option>Mozambique</option>
					<option>Myanmar</option>
					<option>Namibia</option>
					<option>Nauru</option>
					<option>Nepal</option>
					<option>Netherlands</option>
					<option>Netherlands Antilles</option>
					<option>New Caledonia</option>
					<option>New Zealand</option>
					<option>Nicaragua</option>
					<option>Niger</option>
					<option>Nigeria</option>
					<option>Niue</option>
					<option>Norfolk Island</option>
					<option>Northern Mariana Islands</option>
					<option>Norway</option>
					<option>Oman</option>
					<option>Pakistan</option>
					<option>Palau</option>
					<option>Palestinian Territories</option>
					<option>Panama</option>
					<option>Papua New Guinea</option>
					<option>Paraguay</option>
					<option>Peru</option>
					<option>Philippines</option>
					<option>Pitcairn</option>
					<option>Poland</option>
					<option>Portugal</option>
					<option>Puerto Rico</option>
					<option>Qatar</option>
					<option>Runion</option>
					<option>Romania</option>
					<option>Russian Federation</option>
					<option>Rwanda</option>
					<option>Saint Helena</option>
					<option>Saint Kitts and Nevis</option>
					<option>Saint Lucia</option>
					<option>Saint Pierre and Miquelon</option>
					<option>Saint Vincent and the Grenadines</option>
					<option>Samoa</option>
					<option>San Marino</option>
					<option>Sao Tome and Principe</option>
					<option>Saudi Arabia</option>
					<option>Senegal</option>
					<option>Serbia</option>
					<option>Seychelles</option>
					<option>Sierra Leone</option>
					<option>Singapore</option>
					<option>Slovakia</option>
					<option>Slovenia</option>
					<option>Solomon Islands</option>
					<option>Somalia</option>
					<option>South Africa</option>
					<option>South Georgia</option>
					<option>South Sandwich Islands</option>
					<option>Spain</option>
					<option>Sri Lanka</option>
					<option>Sudan</option>
					<option>Suriname</option>
					<option>Svalbard and Jan Mayen Islands</option>
					<option>Swaziland</option>
					<option>Sweden</option>
					<option>Switzerland</option>
					<option>Syria</option>
					<option>Taiwan</option>
					<option>Tajikistan</option>
					<option>Tanzania</option>
					<option>Thailand</option>
					<option>Togo</option>
					<option>Tokelau</option>
					<option>Tonga</option>
					<option>Trinidad and Tobago</option>
					<option>Tunisia</option>
					<option>Turkey</option>
					<option>Turkmenistan</option>
					<option>Turks and Caicos Islands</option>
					<option>Tuvalu</option>
					<option>Uganda</option>
					<option>Ukraine</option>
					<option>United Arab Emirates</option>
					<option>United Kingdom</option>
					<option>United States of America</option>
					<option>Uruguay</option>
					<option>Uzbekistan</option>
					<option>Vanuatu</option>
					<option>Vatican City</option>
					<option>Venezuela</option>
					<option>Vietnam</option>
					<option>Virgin Islands (British)</option>
					<option>Virgin Islands (US)</option>
					<option>Wallis and Futuna Islands</option>
					<option>Western Sahara</option>
					<option>Yemen</option>
					<option>Zaire</option>
					<option>Zambia</option>
					<option>Zimbabwe</option>
				</select>
			</td>
		</tr>
		<tr>
			<td><label for="telephone"><?php echo __( 'Telephone: ', 'privar' ); ?></label></td>
			<td><input type="text" name="mobile" id="telephone" /></td>
		</tr>
		<tr>
			<td><label for="telefax"><?php echo __( 'Telefax: ', 'privar' ); ?></label></td>
			<td><input type="text" name="telephone" id="telefax" /></td>
		</tr>
		<tr>
			<td><label for="mobile"><?php echo __( 'Mobile: ', 'privar' ); ?></label></td>
			<td><input type="text" name="telefax" id="mobile" /></td>
		</tr>
		<tr>
			<td colspan="2">
				<img id="captcha" src="<?php echo plugins_url( 'lib/securimage/securimage_show.php', dirname(__FILE__) ); ?>" alt="CAPTCHA Image" /><br />
				<input type="text" name="captcha_code" size="10" maxlength="6" />
				<input type="button" onclick="document.getElementById('captcha').src = '<?php echo plugins_url( 'lib/securimage/securimage_show.php', dirname(__FILE__) ); ?>?' + Math.random(); return false" value="<?php echo CAPTCHATEXT; ?>" />
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<input type="submit" id="signin" class="button action" value="<?php echo PASIGN; ?>"><br /><br />
				<div class="error-log"></div>
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">

jQuery(document).ready(function($) {

	function checkForm() {
		$('.error-log').html('');
		var checked = true;

		labels = new Array( 'name', 'surname', 'email', 'username', 'password', 'password2', 'company', 'address', 'city', 'zipcode', 'state', 'country', 'telephone', 'captcha_code' );
		for ( var i = 0; i < labels.length; i++) {
			label = "#" + labels[i];
			if ( $(label).val() == '' ) {
				$(label).addClass('error');
				checked = false;
			} else {
				if ( $(label).hasClass('error') ) {
					$(label).removeClass('error');
				}
			}
		}
		if ( !checked ) {
			$('.error-log').html( $('.error-log').html() + "Completa ogni campo del form evidenziato in rosso.<br />");
		}

		if ( $('#password').val() != '' && $('#password2').val() != '') {
			if ( $('#password').val() != $('#password2').val() ) {
				$('#password').addClass('error');
				$('#password2').addClass('error');
				$('.error-log').html( $('.error-log').html() + "Le password non coincidono.<br />");
				checked = false;
			} else {
				$('#password').removeClass('error');
				$('#password2').removeClass('error');
			}
		}

		var reg = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);
		if ( !reg.test( $('#email').val() ) ) {
			$('#email').addClass('error');
			$('.error-log').html( $('.error-log').html() + "Email non corretta.<br />");
			checked = false;
		}

		if ( $("#state").val() != '' ) {
			if ( $("#state").val().length != 2 ) {
				$("#state").addClass('error');
				$('.error-log').html( $('.error-log').html() + "Stato errato.<br />");
				checked = false;
			} else {
				$("#state").removeClass('error');
			}
		}

		if ( $('#telephone').val() != '' ) {
			if ( !jQuery.isNumeric( $('#telephone').val() ) ) {
				$('#telephone').addClass('error');
				checked = false;
				$('.error-log').html( $('.error-log').html() + "Verifica i numeri telefonici inseriti.<br />");
			} else {
				$('#telephone').removeClass('error');
			}
		}

		if ( $('#zipcode').val() != '' ) {
			if ( !jQuery.isNumeric( $('#zipcode').val() ) ) {
				$('#zipcode').addClass('error');
				checked = false;
				$('.error-log').html( $('.error-log').html() + "Verifica i numeri telefonici inseriti.<br />");
			} else {
				$('#zipcode').removeClass('error');
			}
		}

		labels = new Array('telefax', 'mobile');
		for ( var i = 0; i < labels.length; i++ ) {
			label = "#" + labels[i];
			if ( $(label).val() != '' ) {
				if ( !jQuery.isNumeric( $(label).val() ) ) {
					$(label).addClass('error');
					checked = false;
					$('.error-log').html( $('.error-log').html() + "Verifica i numeri telefonici inseriti.<br />");
				} else {
					$(label).removeClass('error');
				}
			} else {
				$(label).removeClass('error');
			}
		}

		return checked;
	}

	$("#privar-signin").submit(function(e) {
		var response  = checkForm();
		if ( response == false ) {
			e.preventDefault();
		}
	});
});
</script>
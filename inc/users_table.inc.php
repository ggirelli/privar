<?php

require_once( dirname(dirname(__FILE__)) . '/lib/wpTable.class.php' );

$args = array(
	'table_name' => 'privar_users',
	'columns' => array(
		'id_users' => __( 'ID', 'privar' ),
		'flag_status' => __( '<center>*</center>', 'privar' ),
		'last_name' => __( 'Last Name', 'privar' ),
		'first_name' => __( 'First Name', 'privar' ),
		'company' => __( 'Company', 'privar' ),
		'email' => __( 'Email', 'privar' ),
		'address' => __( 'Address', 'privar' ),
		'postal_code' => __( 'Zipcode', 'privar' ),
		'city' => __( 'City', 'privar' ),
		'country' => __( 'State', 'privar' ),
		'telephone' => __( 'Telephone', 'privar' ),
		'username' => __( 'Username', 'privar' ),
		'last_login' => __( 'Last login', 'privar' ),
		'flag_drop' => __( 'Drop', 'privar' ),
		'activation' => __( 'Activation', 'privar' ),
		'created_on' => __( 'Created on', 'privar' ),
	),
	'sortable' => array( 'first_name', 'last_name', 'company' ),
	'hidden' => array( 'activation', 'id_users', 'flag_drop', 'created_on' ),
	'hideaway' => array( 'address', 'postal_code', 'city', 'country', 'telephone', 'username', 'last_login' ),
	'id_column' => 'id_users',
	'multi_actions' => array(
		PAUSEROPT1 => '&action=deact',
		PAUSEROPT0 => '&action=act',
		PAUSEROPT2 => '&action=del',
		PAUSEROPT3 => '&action=regen',
	),
	'single_actions' => array(
		PAUSEROPT1 => array(
			'&action=deact&else=%activation%',
			array( 'flag_status', '==', 'a'),
		),
		PAUSEROPT0 => array(
			'&action=act',
			array( 'flag_status', '!=', 'a' ),
		),
		PAUSEROPT2 => array(
			'&action=del',
			array( 'flag_drop', '==', '0' ),
		),
		PAUSEROPT3 => array(
			'&action=regen',
			array( 'flag_drop', '==', '1'),
		),
	),
	'single_action_position' => 'last_name',
	'base_uri' => get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=privar-users',
	'orderby' => 'created_on',
	'order' => 'desc',
	'search_fields' => array( 'first_name', 'last_name', 'email', 'company' ),
	'search_label' => __( 'Search user', 'privar' ),
	'rows_per_page' => '10',
	'allow_edit_rows_per_page' => true,
	'class' => 'users',
);

$t = new WpTable( $args );

?>
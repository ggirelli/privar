<?php
require_once( "users.class.php" );

/**
 * Manages user activation
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class Activator extends Users {

	/**
	 * Tells the class whether to automatically activate the provided code or to wait for manual input
	 * @var boolean
	 */
	public $automatic;

	/**
	 * Whether the provided code has been activated or not
	 * @var boolean
	 */
	public $activated;

	/**
	 * Integer number, error code
	 * @var int
	 */
	public $error;

	/**
	 * Constructor, initialize activation
	 * @return null
	 */
	function __constructor() {
		parent::constructor();

		$this->error = 0;
		$this->activated = false;
		$this->automatic = false;

		$this->init();
	}

	/**
	 * Starts the activation process immediately if everything is correctly set
	 * @return  null
	 */
	private function init() {
		if ( isset( $_GET['code'] ) and @$_GET['code'] != "" ) {
			$this->automatic = true;
			$this->activate( $_GET['code'] );
		} else {
			$this->automatic = false;
		}
	}

	/**
	 * Given an activation code, activates that user
	 * @param  String $code the activation code
	 * @return null
	 */
	public function activate( $code ) {
		if ( parent::isCode( $code ) ) {

			if ( parent::isUserAct( parent::getUserFromCode( $code ) ) ) {
				#Already activated
				$this->activated = true;
				$this->error = 2;
			} else {
				#Activate!
				$ris = parent::activateUser( $code );
				if ( $ris == true ) {
					$this->activated = true;
					$this->error = 0;
				} elseif ( $ris == false ) {
					$this->activated = false;
					$this->error = 3;
				} elseif ( $ris == null ) {
					$this->activated = true;
					$this->error = 4;

					#Email the admin!
					$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
					$headers[] = 'Content-type: text/html';

					$cont = file_get_contents( dirname(dirname(__FILE__)) . '/inc/admin_activation.email.html');
					$cont = str_replace( ADMEMAILKEYWORDTEXT, ADMEMAILTEXT2, $cont );

					wp_mail( get_bloginfo( 'admin_email' ), ADMEMAILSUBJECT2, $cont, $headers );
				}
			}

		} else {
			$this->activated = false;
			$this->error = 1;
		}
	}
}

?>
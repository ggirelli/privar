<?php

/**
 * Class to connect to WordPress DataBase
 *
 * @author  Gabriele Girelli <gabriele@filopoe.it>
 * @copyright  Gabriele Girelli 2014
 * @package  easy-wpdev
 * @version 1.0.0
 */
class DB {

    /**
     * Silence is golden.
     */
    function __construct() {
        /*Silence is golden*/
    }

    /**
     * Updates a database table
     * @param String $table table
     * @param Array $data (column => value) pairs
     * @param Array $format %s as string, %d as integer and %f as float
     * @param Array $where (column => value) pairs for where conditions
     * @param Array $where_format %s as string, %d as integer and %f as float
     * @return Boolean false if anything went wrong
     * @return int the number of updated rows
     */
    public function update( $table, $data, $where, $format, $where_format ) {
        global $wpdb;

        $ris = $wpdb->update( $table, $data, $where, $format, $where_format );

        return $ris;
    }

    /**
     * Verifies if a certain table exists in DB_NAME
     * @param String $table_name table to be checked
     */
    protected function isTable( $table_name ) {
        global $wpdb;

        $sql = "SELECT TABLE_NAME FROM information_schema.tables WHERE TABLE_NAME LIKE '$table_name' AND TABLE_SCHEMA LIKE '" . DB_NAME . "'";
        if ( $wpdb->query( $sql ) > 0 ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Executes a certain query
     * @param String $sql Query to execute
     * @param Boolean $debug Debug mode
     */
    protected function execSQL( $sql, $debug ) {
        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta( $sql );

        if ( $debug ) {
            wp_die();
        }
    }

    /**
     * Executes a certain query and returns the results
     * @param String $sql Quert to execute
     * @return Array of row objects OR null
     */
    protected function getQueryResults( $sql ) {
        global $wpdb;

        $results = $wpdb->get_results( $sql, OBJECT);

        if ( $wpdb->num_rows == 0) {
            return null;
        } else {
            return $results;
        }
    }

    /**
     * Inserts a row in a database table
     * @param String $table table
     * @param Array $data (column => value) pairs
     * @param Array $format %s as string, %d as integer and %f as float
     * @return Boolean false if anything went wrong
     * @return int the id of the inserted row
     */
    protected function insertRow( $table, $data, $format ) {
        global $wpdb;

        $ris = $wpdb->insert( $table, $data, $format );

        if ( !$ris ) {
            return false;
        } else {
            return $wpdb->insert_id;
        }
    }
}

?>
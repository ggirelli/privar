<?php
require_once( "users.class.php" );
require_once( "securimage/securimage.php" );

/**
 * Manages the log-in attempts (to the private area)
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class Logger extends Users{
	/**
	 * Whether the user is logged
	 * @var boolean
	 */
	public $isLogged = false;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();

		$this->isLogged();
	}

	/**
	 * Checks whether the current user is logged and changes this.isLogged accordingly
	 */
	private function isLogged() {
		if( isset($_SESSION[PASLOG]) and @$_SESSION[PASLOG] === PASLOGV ) {
			$this->isLogged = true;
		} else {
			$this->isLogged = false;
		}
	}

	/**
	 * Executes login
	 * @param String $usr username
	 * @param String $pwd password
	 * @return boolean whether login was successful or not
	 */
	public function login( $usr, $pwd ) {
		if ( parent::login( $usr, $pwd ) ) {
			$_SESSION[PASLOG] = PASLOGV;
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the given user is blocked
	 * @param  String  $usr username to check
	 * @return boolean      whether the given user is blocked
	 */
	public function isBlocked( $usr ) {
		$blocked = get_option( 'privar-blocked-login' );
		if ( array_key_exists( $usr, $blocked ) ) {
			if ( $blocked[ $usr ] >= get_option( 'privar-login-attempt' ) )
				return true;
		}
		return false;
	}

	/**
	 * Increment error counter
	 * @param  String $usr given username
	 * @return null
	 */
	public function incrementBlocker( $usr ) {
		$blocked = get_option( 'privar-blocked-login' );
		if ( array_key_exists( $usr, $blocked ) ) {
			$blocked[ $usr ]++;
		} else {
			$blocked[ $usr ] = 1;
		}
		update_option( 'privar-blocked-login', $blocked );
	}

	/**
	 * Empties out the error counter of given user
	 * @param  String $usr given username
	 * @return null
	 */
	public function emptyBlocker( $usr ) {
		$blocked = get_option( 'privar-blocked-login' );
		if ( array_key_exists( $usr, $blocked ) ) {
			$blocked[ $usr ] = 0;
		}
		update_option( 'privar-blocked-login', $blocked );
	}

	/*
	 * Checks if the inserted captcha is ok
	 * @return Boolean
	 */
	public function checkCaptcha() {
		$securimage = new Securimage();
		return $securimage->check($_POST['captcha_code']);
	}
}

?>
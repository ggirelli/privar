<?php

require_once( "db.class.php" );

/**
 * Manages database tables
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class PADB extends DB {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
        $this->init();
    }

    /**
     * Initialize database
     * @return null
     */
    private function init() {
        global $wpdb;
        global $charset_collate;

        $table_name = $wpdb->prefix . "privar_users";

        #If the table does not exist
        if ( !$this->isTable( $table_name ) ) {
            #Create the table

            $sql = "CREATE TABLE $table_name (
                id_users mediumint(8) NOT NULL AUTO_INCREMENT,
                name varchar(50) NOT NULL,
                username varchar(50) NOT NULL,
                password varchar(49) NOT NULL,
                email varchar(50) NOT NULL,
                modules varchar(255) NOT NULL,
                default_language varchar(50) DEFAULT 'it' NOT NULL,
                first_name  varchar(100) NOT NULL,
                last_name varchar(100) NOT NULL,
                company varchar(100) NOT NULL,
                address varchar(100) NOT NULL,
                postal_code varchar(5) NOT NULL,
                city varchar(100) NOT NULL,
                state varchar(2) NOT NULL,
                country varchar(100) NOT NULL,
                telephone varchar(20) NOT NULL,
                telefax varchar(20) NOT NULL,
                mobile varchar(20) NOT NULL,
                vat_id varchar(20) NOT NULL,
                created_on DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                last_login DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
                last_ip varchar(20) NOT NULL,
                activation char(1) DEFAULT '0' NOT NULL,
                activation_code varchar(50) NOT NULL,
                activation_email char(1) DEFAULT '0' NOT NULL,
                flag_status char(1) DEFAULT 'a' NOT NULL,
                flag_drop char(1) DEFAULT '0' NOT NULL,
                PRIMARY KEY  (id_users),
                KEY id_users (id_users)
            ) $charset_collate;";

            $this->execSQL( $sql, false );

        } else {
            #Do nothing
        }
    }

}

?>
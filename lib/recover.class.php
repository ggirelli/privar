<?php
require_once( 'users.class.php' );

/**
 * Manages the password recovery
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class Recover extends Users {

	/**
	 * Id of the user
	 * @var integer
	 */
	private $id;

	/**
	 * Email of the user
	 * @var String
	 */
	private $email;

	/**
	 * Did any errors occur?
	 * @var boolean
	 */
	private $error;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();

		$this->id = false;
		$this->error = false;
	}

	/**
	 * First step to change user password
	 * @param  String $usr username
	 * @param  String $directto redirect
	 * @return null      
	 */
	public function startRecovery( $usr, $directto ) {
		# Recover user ID
		foreach ( $this->ulist as $row ) {
			if ( $row->username == $usr ) {
				$this->id = $row->id_users;
				$this->email = parent::getEmail( $this->id );
			}
		}

		if ( $this->id === false ) {
			#Go back home
			header( "refresh: 1; url='" . $directto . "'" );
			_e( 'Failed to recover password for supplied user.', 'privar' );
		} else {

			$code = $this->mkCode();

			$list = get_option( 'privar-recover-pwd' );
			$list[$this->id] = array(
				'code' => $code,
				'time' => time(),
			);

			if ( $this->is_user_recovering( $this->id ) ) {
				if ( $this->is_code_dead( $this->id ) ) {
					update_option( 'privar-recover-pwd', $list );
				} else {
					#Go back home
					header("refresh: 1; url='" . $directto . "'");
					_e( 'Wait for the previous recovery request to expire (24h).', 'privar' );
					echo "<br />";

					$list = get_option( 'privar-recover-pwd' );
					$code = $list[$this->id]['code'];
				}
			} else {
				update_option( 'privar-recover-pwd', $list );
			}

			$this->send_notice( $code );

			if ( !$this->error ) {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				_e( 'Follow the instructions sent to your email.', 'privar' );
			} else {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				_e( 'Failed to send the email.', 'privar' );
			}
		}
	}

	/**
	 * Second step to change user password
	 * @param  String $code     recovery code
	 * @param  String $usr      username
	 * @param  String $pwd      password
	 * @param  String $pwd2     confirm password
	 * @param  String $directto redirect
	 * @return null           
	 */
	public function endRecovery( $code, $usr, $pwd, $pwd2, $directto ) {
		# Recover user ID
		$this->id = false;
		foreach ( $this->ulist as $row ) {
			if ( $row->username == $usr ) {
				$this->id = $row->id_users;
				$this->email = parent::getEmail( $this->id );
			}
		}	

		if ( $this->is_user_recovering( $this->id ) ) {
			if ( $this->check_user_code( $this->id, $code ) ) {
				if ( !$this->is_code_dead( $this->id ) ) {

					if ( $pwd == $pwd2 ) {

						if ( $this->id === false ) {
							#Go back home
							header("refresh: 1; url='" . $directto . "'");
							_e( 'Failed to recover password for supplied user.', 'privar' );
						} else {
							$r = parent::updatePassword( $this->id, $pwd );
							if ( $r ) {
								$this->error = false;
								$this->confirm_new_password( $pwd );

								if ( !$this->error ) {
									#Go back home
									header("refresh: 1; url='" . $directto . "'");
									_e( 'New password sent to your email.', 'privar' );
								} else {
									#Go back home
									header("refresh: 1; url='" . $directto . "'");
									_e( 'Password updated.', 'privar' );
								}

								#Remove recovery code
								$this->empty_code( $this->id );
							} else {
								#Go back home
								header("refresh: 1; url='" . $directto . "'");
								_e( 'Failed to update your password, try again later.', 'privar' );
							}
						}
					} else {
						#Go back home
						header("refresh: 1; url='" . $directto . "'");
						_e( 'Provided passwords do not coincide.', 'privar' );
					}
				} else {
					#Go back home
					header("refresh: 1; url='" . $directto . "'");
					_e( 'Failed to recover password for supplied user.<br />The code is expired.', 'privar' );
				}
			} else {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				_e( 'Failed to recover password for supplied user.', 'privar' );
			}
		} else {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			_e( 'Failed to recover password for supplied user.', 'privar' );
		}
	}

	/**
	 * Checks if an activation code already exists for the given user
	 * @param  int  $id user id
	 * @return boolean     does a code exist?
	 */
	public function is_user_recovering( $id ) {
		$list = get_option( 'privar-recover-pwd' );
		foreach ( $list as $c => $a ) {
			if ( $c == $id ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Whether a recovery code is dead
	 * @param  String $id user id
	 * @return boolean     true if the code is dead
	 */
	public function is_code_dead( $id ) {
		$list = get_option( 'privar-recover-pwd' );
		if ( $this->is_user_recovering( $id ) ) {
			$diff = time() - $list[$id]['time'];
			if ( $diff >= ( intval( get_option( 'privar-recover-time' ) ) * 60 ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if an activation code already exists
	 * @param  String  $code given code
	 * @return boolean       does the code exist?
	 */
	public function is_code( $code ) {
		$list = get_option( 'privar-recover-pwd' );
		foreach ( $list as $c => $a ) {
			if ( $a['code'] == $code ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks username,code couples
	 * @param  String $id  user id
	 * @param  String $code recovery code
	 * @return boolean       if the couple exists
	 */
	public function check_user_code( $id, $code ) {
		# Check code
		$list = get_option( 'privar-recover-pwd' );
		if ( $list[$id]['code'] == $code ) {
			return true;
		}
		return false;
	}

	/**
	 * Remvoes the recovery code of the given user
	 * @param  String $id user id
	 * @return null     
	 */
	public function empty_code( $id ) {
		$list = get_option( 'privar-recover-pwd' );
		unset( $list[$id] );
		update_option( 'privar-recover-pwd', $list );
	}

	/**
	 * Prepares a unique activation code
	 * @return String the unique activation code
	 */
	private function mkCode() {
		$code = $this->random_string( 25 );
		while ( $this->is_code( $code ) ) {
			$code = $this->random_string( 25 );
		}
		return $code;
	}

	/**
	 * Creates a random string with given length
	 * @param int $l the desired length
	 * @return String
	 * @author  Gabriele Girelli <gabriele@filopoe.it>
	 * @copyright  Gabriele Girelli 2014
	 * @package easy-wpdev
	 */
	private function random_string( $l ) {
		$s = "";

		# Generates a random string
		# (which length is a multiple of 32)
		for ( $i = 0; $i <= $l/32; $i++ )
			$s .= md5( time() + rand( 0, 99 ) );

		# Then shorten the random string
		# to the desired length
		while ( strlen( $s ) > $l ) {
			$i = (int) rand( 1, strlen( $s ) );
			$s = substr( $s, 0, $i ) . substr( $s, $i+1, strlen( $s ) - $i - 1 );
		}

		return $s;
	}

	/**
	 * Send the link to recover the password to the user
	 * @param  String $code the recovery code
	 * @return null       Sets $this->error true if the email was not sent
	 */
	private function send_notice( $code ) {
		# Email the user with the code to actirecover the password
		$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
		$headers[] = 'Content-type: text/html';

		$body = file_get_contents( dirname(dirname(__FILE__)) . '/inc/recover.email.html');
		$link = plugins_url( 'privar-recover.php', dirname(__FILE__) ) . "?code=" . $code;
		$body = str_replace( EMAILKEYWORDLINK, $link, $body );
		$body = str_replace( EMAILKEYWORDTEXT, RECEMAILTEXT, $body );
		$body = eregi_replace("[\]",'',$body);

		if ( get_option( 'privar-smtp-check' ) ) {
			require_once('PHPMailer/class.phpmailer.php');
			$mail = new PHPMailer();

			$mail->ClearAllRecipients();
			$mail->ClearAddresses();
			$mail->ClearAttachments();
			$mail->CharSet = 'utf-8';

			$mail->IsSMTP();
			$mail->Host = get_option( 'privar-smtp-server' );
			$mail->SMTPDebug = 0;

			if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
				$mail->SMTPSecure = "ssl";
			} elseif ( get_option( 'privar-smtp-conn' ) == 'tsl' ) {
				$mail->SMTPSecure = "tsl";
			}

			if ( get_option( 'privar-smtp-username' ) != "" and get_option( 'privar-smtp-password' ) != "" ) {
				$mail->SMTPAuth = true;
				$mail->Username = get_option( 'privar-smtp-username' );
				$mail->Password = get_option( 'privar-smtp-password' );
			}

			$mail->Port = get_option( 'privar-smtp-port' );
			
			$mail->SetFrom( get_option( 'privar-sign-email-sender-address' ) );
			$mail->Subject = RECEMAILSUBJECT;
			$mail->MsgHTML($body);

			$mail->AddAddress( $this->email );
			$mail->AddReplyTo( get_option( 'privar-sign-email-sender-address' ) );

			if(!$mail->Send()) {
				$this->error = true;
				echo "Mailer Error: " . $mail->ErrorInfo . "<br />";
			}
		} else {
			wp_mail( $this->email, RECEMAILSUBJECT, $cont, $headers );
		}
	}

	/**
	 * Conferma di cambiamento password
	 * @param  String $pwd nuova password
	 * @return null      Sets $this->error true if the email was not sent
	 */
	private function confirm_new_password( $pwd ) {
		# Email the user with the code to actirecover the password
		$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
		$headers[] = 'Content-type: text/html';

		$body = file_get_contents( dirname(dirname(__FILE__)) . '/inc/recover.email.html');
		$link = get_bloginfo( 'wpurl' );
		$body = str_replace( EMAILKEYWORDLINK, $link, $body );
		$body = str_replace( EMAILKEYWORDTEXT, RECEMAILTEXT2 . ': ' . $pwd, $body );
		$body = eregi_replace("[\]",'',$body);

		if ( get_option( 'privar-smtp-check' ) ) {
			require_once('PHPMailer/class.phpmailer.php');
			$mail = new PHPMailer();

			$mail->ClearAllRecipients();
			$mail->ClearAddresses();
			$mail->ClearAttachments();
			$mail->CharSet = 'utf-8';

			$mail->IsSMTP();
			$mail->Host = get_option( 'privar-smtp-server' );
			$mail->SMTPDebug = 0;

			if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
				$mail->SMTPSecure = "ssl";
			} elseif ( get_option( 'privar-smtp-conn' ) == 'tsl' ) {
				$mail->SMTPSecure = "tsl";
			}

			if ( get_option( 'privar-smtp-username' ) != "" and get_option( 'privar-smtp-password' ) != "" ) {
				$mail->SMTPAuth = true;
				$mail->Username = get_option( 'privar-smtp-username' );
				$mail->Password = get_option( 'privar-smtp-password' );
			}

			$mail->Port = get_option( 'privar-smtp-port' );
			
			$mail->SetFrom( get_option( 'privar-sign-email-sender-address' ) );
			$mail->Subject = RECEMAILSUBJECT;
			$mail->MsgHTML($body);

			$mail->AddAddress( $this->email );
			$mail->AddReplyTo( get_option( 'privar-sign-email-sender-address' ) );

			if(!$mail->Send()) {
				$this->error = true;
				echo "Mailer Error: " . $mail->ErrorInfo . "<br />";
			}
		} else {
			wp_mail( $this->email, RECEMAILSUBJECT, $cont, $headers );
		}
	}
}

?>

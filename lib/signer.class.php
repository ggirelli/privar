<?php
require_once( "users.class.php" );
require_once( "securimage/securimage.php" );

/**
 * Manages the sign-in attempts (to the private area)
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class Signer extends Users {

	/**
	 * Fields of the sign-in form
	 * @var array
	 */
	private $fields = array( 'name', 'surname', 'email', 'username', 'password', 'password2', 'company', 'address', 'city', 'zipcode', 'state', 'country', 'telephone', 'telefax', 'mobile');

	function __construct() {
		parent::__construct();
	}

	/**
	 * Checks for captcha
	 * @return boolean captcha answer
	 */
	public function checkCaptcha() {
		$securimage = new Securimage();
		return $securimage->check($_POST['captcha_code']);
	}

	/**
	 * Checks data from form
	 * @return int coded error
	 */
	public function checkFormData() {
		#General check
		foreach ( $this->fields as $field ) {
			if ( !isset($_POST[$field]) ) { return 0; }
			if ( @$_POST[$field] == "" and !in_array( $field, array('telephone','telefax','mobile') ) ) { return 0; }
		}

		#Check State length
		if ( strlen( $_POST['state'] ) != 2 ) { return 1; }

		#Check email
		if ( !filter_var( $_POST['email'], FILTER_VALIDATE_EMAIL ) ) { return 2; }

		#Check password
		if ( $_POST['password'] != $_POST['password2'] ) { return 3; }

		#Check user
		if ( parent::isUser( $_POST['username'] ) ) { return 4; }

		#Check telephone
		if ( @$_POST['telephone'] != "" and !is_numeric($_POST['telephone']) ) { return 5; }
		#Check telefax
		if ( @$_POST['telefax'] != "" and !is_numeric($_POST['telefax']) ) { return 6; }
		#Check mobile
		if ( @$_POST['mobile'] != "" and !is_numeric($_POST['mobile']) ) { return 7; }
		#Check zipcode
		if ( @$_POST['zipcode'] != "" and !is_numeric($_POST['zipcode']) ) { return 8; }

		#Check email
		if ( parent::isEmail( $_POST['email'] ) ) { return 9; }

		return 10;
	}

	/**
	 * Converts POST data into an array
	 * @return Array POST data
	 */
	public function formToArray() {
		$a = array();
		foreach ( $this->fields as $field ) {
			$a[$field] = $_POST[$field];
		}

		$length = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$emailcode = '';
		for ($p = 0; $p < $length; $p++) {
			$emailcode .= $characters[mt_rand(0, strlen($characters))];
		}
		while ( parent::isCode( $emailcode ) ) {
			$emailcode = '';
			for ($p = 0; $p < $length; $p++) {
				$emailcode .= $characters[mt_rand(0, strlen($characters))];
			}
		}
		$a['activation_email'] = $emailcode;
		
		return $a;
	}


	/**
	 * Performs complete sign in
	 * @return boolean signed in?
	 * @return String error message
	 */
	public function signin() {
		$data = $this->formToArray();
		$ris = parent::signin( $data );

		if ( $ris == false ) {
			return $ris;
		} else {
			#Data inserted correctly into the database

			#Email the new user with the code to activate the account
			$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
			$headers[] = 'Content-type: text/html';

			$body = file_get_contents( dirname(dirname(__FILE__)) . '/inc/user.email.html');
			$link = plugins_url( 'privar-activator.php', dirname(__FILE__) ) . "?code=" . $data['activation_email'];
			$body = str_replace( EMAILKEYWORDLINK, $link, $body );
			$body = str_replace( EMAILKEYWORDTEXT, EMAILTEXT, $body );
			$body = eregi_replace("[\]",'',$body);

			if ( get_option( 'privar-smtp-check' ) ) {
				require_once('PHPMailer/class.phpmailer.php');
				$mail = new PHPMailer();

				$mail->ClearAllRecipients();
				$mail->ClearAddresses();
				$mail->ClearAttachments();
				$mail->CharSet = 'utf-8';

				$mail->IsSMTP();
				$mail->Host = get_option( 'privar-smtp-server' );
				$mail->SMTPDebug = 0;

				if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
					$mail->SMTPSecure = "ssl";
				} elseif ( get_option( 'privar-smtp-conn' ) == 'tsl' ) {
					$mail->SMTPSecure = "tsl";
				}

				if ( get_option( 'privar-smtp-username' ) != "" and get_option( 'privar-smtp-password' ) != "" ) {
					$mail->SMTPAuth = true;
					$mail->Username = get_option( 'privar-smtp-username' );
					$mail->Password = get_option( 'privar-smtp-password' );
				}

				$mail->Port = get_option( 'privar-smtp-port' );
				
				$mail->SetFrom( get_option( 'privar-sign-email-sender-address' ) );
				$mail->Subject = EMAILSUBJECT;
				$mail->MsgHTML($body);

				$mail->AddAddress( $data['email'] );
				$mail->AddReplyTo( get_option( 'privar-sign-email-sender-address' ) );

				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo . "<br />";
				}
			} else {
				wp_mail( $data['email'], EMAILSUBJECT, $cont, $headers );
			}

			// #Email the admin!
			$headers[] = 'From: ' . get_option( 'privar-sign-email-sender-name' ) . ' <' . get_option( 'privar-sign-email-sender-address' ) . '>' . "\r\n";
			$headers[] = 'Content-type: text/html';

			$body = file_get_contents( dirname(dirname(__FILE__)) . '/inc/admin.email.html');
			$link = plugins_url( 'privar-activator.php', dirname(__FILE__) ) . "?code=" . $data['activation_email'];
			$body = str_replace( ADMEMAILKEYWORDTEXT, ADMEMAILTEXT, $body );
			$text = $data['name'] . " " . $data['surname'];
			$text .= "<br />\n" . $data['company'];
			$text .= "<br />\nEmail: " . $data['email'];
			$text .= "<br />\nPhone: " . $data['telephone'];
			$text .= "<br />\nAddress: " . $data['address'] . ", " . $data['city'] . " - " . $data['zipcode'] . " - " . $data['country'] . " (" . $data['state'] . ")";
			$body = str_replace( ADMEMAILKEYWORDTEXT2, $text, $body );
			$body = eregi_replace("[\]",'',$body);

			if ( get_option( 'privar-smtp-check' ) ) {
				require_once('PHPMailer/class.phpmailer.php');
				$mail = new PHPMailer();

				$mail->ClearAllRecipients();
				$mail->ClearAddresses();
				$mail->ClearAttachments();
				$mail->CharSet = 'utf-8';

				$mail->IsSMTP();
				$mail->Host = get_option( 'privar-smtp-server' );
				$mail->SMTPDebug = 0;

				if ( get_option( 'privar-smtp-conn' ) == 'ssl' ) {
					$mail->SMTPSecure = "ssl";
				} elseif ( get_option( 'privar-smtp-conn' ) == 'tsl' ) {
					$mail->SMTPSecure = "tsl";
				}
				

				if ( get_option( 'privar-smtp-username' ) != "" and get_option( 'privar-smtp-password' ) != "" ) {
					$mail->SMTPAuth = true;
					$mail->Username = get_option( 'privar-smtp-username' );
					$mail->Password = get_option( 'privar-smtp-password' );
				}

				$mail->Port = get_option( 'privar-smtp-port' );

				$mail->SetFrom( get_option( 'privar-sign-email-sender-address' ) );
				$mail->Subject = EMAILSUBJECT;
				$mail->MsgHTML($body);

				$mail->AddAddress( get_option( 'privar-sign-email-admin-address' ) );
				$mail->AddReplyTo( get_option( 'privar-sign-email-sender-address' ) );

				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo . "<br />";
				}
			} else {
				wp_mail( get_bloginfo( 'admin_email' ), ADMEMAILSUBJECT, $cont, $headers );
			}

			return true;
		}
	}
}

?>
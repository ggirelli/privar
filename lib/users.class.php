<?php
require_once( "db.class.php" );

/**
 * Manages the users
 * 
 * @author Gabriele Girelli <gabriele@filopoe.it>
 * @version 1.0.0
 * @copyright Gabriele Girelli 2014
 */
class Users extends DB {

	/**
	 * Plugin main database table name
	 * @var String
	 */
	public $table_name;

	/**
	 * List of user data from database table fields
	 * @var Array
	 */
	public $ulist;

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();

		global $wpdb;
		$this->table_name = $wpdb->prefix . "privar_users";
		$this->getUsers();
	}

	/**
	 * Checks whether the given users exists or not
	 * @param String $usr Username
	 * @return boolean
	 */
	public function isUser( $usr ) {
		$sql = "SELECT username FROM $this->table_name WHERE username LIKE '$usr'";

		$list = parent::getQueryResults( $sql );

		if ( $list != null and count($list) == 1 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the given user has activated its account
	 * @param String $usr username
	 * @return boolean was cactivated?
	 */
	public function isUserAct( $usr ) {
		if ( $this->isUserConf( $usr ) ) {
			return true;
		} else {
			$sql = "SELECT flag_status FROM $this->table_name WHERE username LIKE '$usr'";

			$list = parent::getQueryResults( $sql );

			foreach ( $list as $row ) {
				if ( $row->flag_status == 'i' ) {
					return true;
				} else {
					return false;
				}
			}
		}
	}

	/**
	 * Checks if the given user was confirmed
	 * @param String $usr username
	 * @return boolean was confirmed?
	 */
	public function isUserConf( $usr ) {
		$sql = "SELECT flag_status FROM $this->table_name WHERE username LIKE '$usr'";

		$list = parent::getQueryResults( $sql );

		foreach ( $list as $row ) {
			if ( $row->flag_status == 'a' ) {
				return true;
			} else {
				return false;
			}
		}
	}

	/**
	 * Checks if there are error in the given user database row
	 * @param String $usr username
	 * @return boolean is the user ok?
	 */
	public function checkUser( $usr ) {
		$sql = "SELECT activation, activation_email, flag_status, flag_drop FROM $this->table_name WHERE username LIKE '$usr'";

		$list = parent::getQueryResults( $sql );

		foreach ( $list as $row ) {
			if ( !in_array( $row->activation, array('0','1') ) ) { return false; }
			if ( !in_array( $row->activation_email, array('0','1') ) ) { return false; }
			if ( !in_array( $row->flag_status, array('w','i','a') ) ) { return false; }
			if ( !in_array( $row->flag_drop, array('0','1') ) ) { return false; }

			#Check activation
			if( $row->flag_status != 'a' ) {
				if ( $row->activation == '0' ) {

					#Check activation_email
					if ( $row->activation_email == '0' ) {

						if ( $row->flag_status == 'w' ) { return true; }
						else { return false; }

					} elseif ( $row->activation_email == '1' ) {

						if ( $row->flag_status == 'a' ) { return true; }
						else { return false; }

					} else { return false; }

				} elseif ( $row->activation == '1' ) {

					#Check activation_email
					if ( $row->activation_email == '0' ) {

						if ( $row->flag_status == 'i' ) { return true; }
						else { return false; }

					} elseif ( $row->activation_email == '1' ) {

						if ( $row->flag_status == 'a' ) { return true; }
						else { return false; }

					} else { return false; }

				} else { return false; }
			} else { return true; }
		}
	}

	/**
	 * Checks if the given user was dropped
	 * @param String $usr username
	 * @return boolean was the user dropped?
	 * @return null if something went wrong
	 */
	public function isUserDropped( $usr ) {
		$sql = "SELECT flag_drop FROM $this->table_name WHERE username LIKE '$usr'";

		$list = parent::getQueryResults( $sql );

		foreach ( $list as $u ) {
			if ( $u->flag_drop == '0' ) {
				return false;
			} elseif ( $u->flag_drop == '1' ) {
				return true;
			} else {
				return null;
			}
		}
	}

	/**
	 * Checks whether the email activation code has already been assigned to an user or not
	 * @param String $code the email activation code
	 * @return boolean
	 */
	public function isCode( $code ) {
		$sql = "SELECT activation_code FROM $this->table_name WHERE activation_code LIKE '$code'";

		$list = parent::getQueryResults( $sql );

		if ( count( $list ) != 1 ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Checks whether the email has already been assigned to an user or not
	 * @param String $code the email
	 * @return boolean
	 */
	public function isEmail( $email ) {
		$sql = "SELECT email FROM $this->table_name WHERE email LIKE '$email'";

		$list = parent::getQueryResults( $sql );

		if ( count( $list ) == 0 ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Gets the email from the user id
	 * @param String $userid id_user
	 * @return String the email
	 */
	public function getEmail( $userid ) {
		$sql = "SELECT email FROM $this->table_name WHERE id_users LIKE '$userid'";

		$list = parent::getQueryResults( $sql );

		if ( count( $list ) != 1 ) {
			return false;
		} else {
			foreach ( $list as $usr ) {
				return $usr->email;
			}
		}
	}

	/**
	 * Gets the username from the email
	 * @param String $email email to identify user
	 * @return String the username
	 */
	public function getUserByEmail( $email ) {
		$sql = "SELECT username FROM $this->table_name WHERE email LIKE '$email'";

		$list = parent::getQueryResults( $sql );

		if ( count( $list ) != 1 ) {
			return false;
		} else {
			foreach ( $list as $usr ) {
				return $usr->username;
			}
		}
	}

	/**
	 * Gets the username from the activation code
	 * @param String $code activation_code to identify user
	 * @return String the username
	 */
	protected function getUserFromCode( $code ) {
		$sql = "SELECT username FROM $this->table_name WHERE activation_code LIKE '$code'";

		$list = parent::getQueryResults( $sql );

		if ( count( $list ) != 1 ) {
			return false;
		} else {
			foreach ( $list as $usr ) {
				return $usr->username;
			}
		}
	}

	/**
	 * Activate a user given the activation code
	 * @param String $code the activation_code
	 * @return boolean if the user was activated or not
	 * @return null if anything went wrong
	 */
	protected function activateUser( $code ) {
		$data = array(
			'activation' => '1',
			'flag_status' => 'i'
		);
		$format = array( '%s', '%s' );
		$where = array( 'activation_code' => $code );
		$where_format = array( '%s' );
		
		$ris = parent::update( $this->table_name, $data, $where, $format, $where_format );

		if ( $ris == 1 ) {
			return true;
		} elseif ( $ris == false ) {
			return $ris;
		} else {
			return $ris;
		}
	}

	/**
	 * Checks if given credentials allow a correct login
	 * @param String $usr username
	 * @param String $pwd password
	 * @return boolean
	 * @return null if something went wrong
	 */
	protected function login( $usr, $pwd ) {
		$pwd = md5($pwd);
		$sql = "SELECT id_users FROM $this->table_name WHERE username LIKE '$usr' AND password LIKE '$pwd'";

		$list = parent::getQueryResults( $sql );
		
		if ( count( $list ) == 0 ) {
			return false;
		} elseif ( count( $list ) == 1 ) {
			$data = array( 'last_login' => date('Y-m-d H:i:s') );
			$where = array( 'username' => $usr, 'password' => $pwd );
			$format = array( '%s' );
			$where_format = array( '%s', '%s' );
			parent::update( $this->table_name, $data, $where, $format, $where_format );
			return true;
		} else {
			return null;
		}
	}

	/**
	 * Executes sign in (only at database level)
	 * @param array $formdata (key => val) where key is the form field name and val the value
	 * @return int if sign in was successful
	 * @return boolean (false) if something went wrong
	 */
	protected function signin( $formdata ) {
		#Set data to insert into table
		$data = array(
			'name' => $formdata['name'],
			'username' => $formdata['username'],
			'password' => md5($formdata['password']),
			'email' => $formdata['email'],
			'first_name' => $formdata['name'],
			'last_name' => $formdata['surname'],
			'company' => $formdata['company'],
			'address' => $formdata['address'],
			'postal_code' => $formdata['zipcode'],
			'city' => $formdata['city'],
			'state' => $formdata['state'],
			'country' => $formdata['country'],
			'telephone' => $formdata['telephone'],
			'telefax' => $formdata['telefax'],
			'mobile' => $formdata['mobile'],
			'created_on' => date("Y-m-d H:i:s"),
			'activation' => '0',
			'activation_code' => $formdata['activation_email'],
			'activation_email' => '0',
			'flag_status' => 'w',
			'flag_drop' => '0'
		);

		#Set the data format
		$format = array( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d', '%s', '%d' );

		#Insert data
		return parent::insertRow( $this->table_name, $data, $format );
	}

	/**
	 * Updates the user password based on user's ID
	 * @param  String $id  user ID
	 * @param  String $pwd new password
	 * @return boolean      whether the update was successful
	 */
	protected function updatePassword( $id, $pwd ) {
		$data = array( 'password' => md5($pwd), );
		$where = array( 'id_users' => $id, );
		$format = array( '%s' );

		$ris = parent::update( $this->table_name, $data, $where, $format, $format);

		if ( $ris == 1 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Provides username, name, surname, flag_status and flag_drop of every user
	 */
	private function getUsers() {
		$sql = "SELECT id_users, username, first_name, last_name, activation, flag_status, flag_drop FROM $this->table_name";
		$this->ulist = parent::getQueryResults( $sql );
	}

}

?>
<?php
require_once( 'db.class.php' );

/**
 * Class to build WordPress-styled tables
 *
 * @author  Gabriele Girelli <gabriele@filopoe.it>
 * @copyright  Gabriele Girelli 2014
 * @package  easy-wpdev
 * @version	1.0.1
 */
class WpTable extends DB {
	private $checked;
	private $args, $checkbox;
	private $table;
	private $sorted, $orderby, $order;
	private $nrows, $page, $paged, $perpage;
	private $searched, $s;
	
	/**
	 * Initialize the class
	 * @param Array $arg arguments' array<br />
	 * 	<ul>
	 *		<li>String/Array 	<b>'table_name'</b> 	database table name or hard-copy table 	-	<b>optional</b></li>
	 *		<li>boolean 	<b>'is_table'</b> 	if 'table_name' is an hard-copy table 	- 	<b>optional</b></li>
	 *		<li>Array 	<b>'columns'</b> 	array of couples (col_name => col_label) 	-	<b>required</b></li>
	 *		<li>Array 	<b>'sortable'</b> 	sortable columns' array (col_names) 	-	<b>optional</b></li>
	 *		<li>Array 	<b>'hidden'</b> 	hidden columns' array (col_names) 	-	<b>optional</b></li>
	 *		<li>Array 	<b>'hideaway'</b> 	hideable coulmns' array (col_names) 	-	<b>optional</b></li>
	 *		<li>String 	<b>'id_column'</b> 	ID column's col_name 	-	<b>required</b></li>
	 *		<li>Array 	<b>'multi_actions'</b> 	couples' array (Action => URI), can be performed on multiple rows 	-	<b>optional</b></li>
	 *		<li>Array 	<b>'single_actions'</b> 	couples' array (Action => URI), can be performed on a row 	-	<b>optional</b></li>
	 *		<li>String 	<b>'single_action_position'</b> 	single_action column position (col_name) 	-	<b>optional</b></li>
	 *		<li>Array 	<b>'single_action_condition'</b> 	conditions' array for each single_action to be showed 	-	<b>optional</b></li>
	 *		<li>String 	<b>'base_uri'</b> 	URI of the page that will contain the table 	-	<b>required</b></li>
	 *		<li>String 	<b>'orderby'</b> 	field for default ordering 	-	<b>optional</b></li>
	 *		<li>String 	<b>'order'</b> 	ASC or DESC order 	-	<b>optional</b></li>
	 *		<li>Array 	<b>'search_fields'</b> 	fields in which the user can search for keyword(s) 	-	<b>optional</b></li>
	 *		<li>String 	<b>'search_label'</b> 	label for the search field 	-	<b>optional</b></li>
	 *		<li>integer 	<b>'rows_per_page'</b> 	number of rows per page, activates pagination	-	<b>optional</b></li>
	 *		<li>boolean 	<b>'checkbox'</b> 	whether to create a checkbox-column or not, <b>default: true</b> 	-	<b>optional</b></li>
	 * 	</ul>
	 */
	function __construct( $args ) {
		parent::__construct();

		// Check arguments
		$this->checked = WpTable::check_args( $args );
		if ( $this->checked ) {
			// Make arguments local
			$this->args = $args;
			$this->checkbox = $this->get_arg( 'checkbox' );

			// Check for $_GET
			# Sorted
			if ( isset( $_GET['orderby'] ) and isset( $_GET['order'] ) ) {
				$this->sorted = true;
				$this->orderby = $_GET['orderby'];
				$this->order = $_GET['order'];
			} else {
				$this->sorted = false;
			}
			# Pagination
			if ( isset( $_GET['edit_post_per_page'] ) and @$_GET['edit_post_per_page'] != '') {
				$this->perpage = $_GET['edit_post_per_page'];
			} else {
				$this->perpage = false;
			}
			# Paginated
			if ( isset( $_GET['paged'] ) ) {
				$this->page = $_GET['paged'];
				$this->paged = true;
			} else {
				$this->paged = false;
			}
			# Serached
			if ( isset( $_GET['s'] ) and @$_GET['s'] != '' ) {
				$this->searched = true;
				$this->s = $_GET['s'];
			} else {
				$this->searched = false;
			}

			// Check for $_POST
			# Pagination
			if ( !isset( $_GET['edit_post_per_page'] ) ) {
				if ( isset( $_POST['edit_post_per_page'] ) and @$_POST['edit_post_per_page'] != '') {
					$this->perpage = $_POST['edit_post_per_page'];
				} else {
					$this->perpage = false;
				}
			}

			// Print table
			$this->print_table();
		}
	}
	
	/**
	 * Builds and prints the actual WordPress-styled table
	 * @return none just execute and print
	 */
	private function print_table() {
		if ( $this->get_arg( 'is_table' ) ) {
			$rows = $this->get_arg( 'table_name' );

			if ( $this->sorted ) {
				$rows = $this->sort_rows( $rows );
			}
		} else {
			$rows = $this->query_rows();
		}

		$this->table = '';

		$this->make_portion( 'pre' );

		$this->table .= '<table class="wp-list-table widefat' . $this->get_arg( 'class' ) . '">';
		$this->make_portion( 'head' );

		$this->make_rows( $rows );

		$this->make_portion( 'foot' );
		$this->table .= '</table>';

		$this->make_portion( 'post' );

		$this->make_portion( 'last' );

		echo $this->table;
	}
	
	/**
	 * Gets the rows from the database
	 * @return Array the rows' array, each row is a stdClass object
	 */
	private function query_rows() {
		global $wpdb;

		$sql = '';
		foreach ( $this->get_arg( 'columns' ) as $key => $label ) {
			if ( $sql == '' ) {
				$sql .= "SELECT $key";
			} else {
				$sql .= ", $key";
			}
		}
		$sql .= ' FROM ' . $wpdb->prefix . $this->get_arg( 'table_name' );

		// Searched?
		$sfs = $this->get_arg( 'search_fields' );
		if ( $this->searched and !empty( $sfs )) {
			$search_ql .= " WHERE (";

			for ( $i = 0; $i < count( $sfs ); $i++ ){
				$f = $sfs[ $i ];

				$search_ql .= " $f LIKE '%" . esc_sql($this->s) . "%'";

				if ( $i != ( count( $sfs ) -1 ) ) { $search_ql .= " OR"; }
			}

			$search_ql .= " )";
		}
		$sql .= $search_ql;

		// Sorted?
		if ( !$this->sorted ) {
			$orderby = $this->get_arg( 'orderby' );
			$order = $this->get_arg( 'order' );
		} else {
			$orderby = $this->orderby;
			$order = $this->order;
		}
		if ( $orderby != "" and in_array( $order, array( 'asc', 'desc', 'ASC', 'DESC' ) ) ) {
			$sql .= " ORDER BY $orderby $order";
		}

		// Paginated?
		$rpp = $this->get_arg( 'rows_per_page' );
		if ( $rpp != "" and !$this->paged ) {
			$sql .= " LIMIT $rpp";
		} elseif ( $rpp != "" and $this->paged ) {
			$sql .= " LIMIT " . $rpp * ($this->page - 1) . ",$rpp";
		}

		// Get total number of rows
		$this->nrows = parent::getQueryResults( "SELECT COUNT(*) as n FROM " . $wpdb->prefix . $this->get_arg( 'table_name' ) . $search_ql );
		$this->nrows = $this->nrows[0]->n;

		$rows = parent::getQueryResults( $sql );

		return $rows;
	}

	/**
	 * Builds the non-header rows of the table
	 * @param  Array $rows Array of stdClass object rows, returned by mysql query or supplied as hard-copy table
	 * @return none       it appends to $this->table the rows
	 */
	private function make_rows( $rows ) {

		if ( count( $rows ) != 0) {
			foreach ( $rows as $row ) {
				eval ( "\$id = \$row->" . $this->get_arg( 'id_column' ) . ";" );
				$this->table .= "<tr>";
				
				if ( $this->checkbox ) {
					// Checkbox
					$this->table .= "<th class='check-column' scope='row'>";
					$this->table .= "<label class='screen-reader-text' for='cb-select-" . $row->id_users . "'>";
					$this->table .= __( 'Seleziona ', 'privar' ) . $row->username . "</label>";
					$this->table .= "<input id='cb-select-" . $row->id_users . "' type='checkbox' value='" . $row->id_users;
					$this->table .= "' name='" . $row->username . "' />";
					$this->table .= "</th>";
				}

				// Columns
				foreach ( $this->get_arg( 'columns' ) as $name => $label ) {
					if ( !in_array( $name, $this->get_arg( 'hidden' ) ) ) {
						$this->table .= "<td class='" . $name;

						// If hideaway
						if ( in_array( $name, $this->get_arg( 'hideaway' ) ) ) {
							$this->table .= ' hideaway';
						}

						$this->table .= "'>" . $row->$name;

						// Single actions
						$sas = $this->get_arg( 'single_actions' );
						if ( $name == $this->get_arg( 'single_action_position' ) and !empty( $sas ) ) {
							$this->table .= "<div class='row-actions'>";
							foreach ( $this->get_arg( 'single_actions' ) as $action => $opt ) {
								
								if ( count( $opt ) == 2 ) {
									eval( "\$cond = \$row->" . $opt[1][0] . " " . $opt[1][1] . " '" . $opt[1][2] . "';" );
									if ( $cond ) {
										$this->table .= "<span class='$action'><a href='" . $this->get_arg( 'base_uri' ) . $this->parse_uri( $opt[0], $row ) . "&id=$id&edit_post_per_page=" . $this->perpage . "'>$action</a></span>|";
									}
								} else {
									$this->table .= "<span class='$action'><a href='" . $this->get_arg( 'base_uri' ) . $this->parse_uri( $opt[0], $row ) . "&id=$id&edit_post_per_page=" . $this->perpage . "'>$action</a></span>|";
								}
							}
							$this->table = substr( $this->table, 0, -1 );
							$this->table .= "</div>";
						}

						$this->table .= "</td>";
					}
				}
				$this->table .= "</tr>";
			}
		}
	}
	
	/**
	 * Parses an in-row uri adding data to it, if required
	 * @param  String $uri the URI to be parsed
	 * @param  stdClass $row the row containing data to add
	 * @return String      the parsed URI
	 */
	private function parse_uri( $uri, $row ) {
		$columns = array();
		foreach ( $this->get_arg( 'columns' ) as $key => $var ) {
			array_push( $columns, $key);
		}

		$a_uri = explode( '%', $uri );
		$uri = '';
		foreach ( $a_uri as $frag ) {
			if ( in_array( $frag, $columns ) ) {
				eval( "\$uri .= \$row->$frag;" );
			} else {
				$uri .= $frag;
			}
		}
		return $uri;
	}
	
	/**
	 * Sorts rows if a hard-copy table was supplied in args
	 * @param  Array $rows Array of rows, each row is a stdClass object
	 * @return Array       The sorted rows' array
	 */
	private function sort_rows( $rows ) {
		$sorting = array();

		for ( $i = 0; $i < count( $rows ); $i++ ) {
			$row = $rows[ $i ];
			eval ( "\$id = \$row->" . $this->get_arg( 'id_column' ) . ";" );
			eval ( "\$orderby = \$row->" . $this->orderby . ";" );
			eval ( "\$sorting[$id] = '$orderby';" );
		}

		switch ( $this->order ) {
			case 'asc': case 'ASC': {
				asort( $sorting );
				break;
			}
			case 'desc': case 'DESC': {
				arsort( $sorting );
				break;
			}
		}

		$sorted = array();
		foreach ( $sorting as $k => $v ) {
			array_push( $sorted, $rows[ $k ] );
		}

		return $sorted;
	}

	/**
	 * Builds specific portions of the table
	 * @param  String $portion the name of the portion
	 * @return none          it appends to $this->table the portion
	 */
	private function make_portion( $portion ) {
		switch ( $portion ) {
			case 'pre': {

				// Page options
				if ( $this->get_arg('allow_edit_rows_per_page') ) {
					$this->make_portion( 'page-options' );
				}

				// Search box
				$searches = $this->get_arg( 'search_fields' );
				if ( !empty( $searches ) ) {
					$this->table .= '<form id="search-box" method="post" data-target="' . $this->get_arg( 'base_uri' ) . '"><p class="search-box">';
					$this->table .= '<input id="post-search-input" type="search" name="s"></input>';
					$this->table .= '<input id="search-submit" class="button" type="submit" value="' . $this->get_arg( 'search_label' ) . '" name=""></input>';
					$this->table .= "</p></form>";
				}

				// Tablenav top - De-activated if hard table is provided
				if ( !$this->get_arg( 'is_table' ) ) {
					$this->table .= '<div class="tablenav top">';

					$this->table .= "<div class='alignleft actions'>";
					// If there are hideaways
					$hideaway = $this->get_arg( 'hideaway' );
					if ( !empty( $hideaway ) ) {
						$this->table .= '<input type="button" class="button action expand-button" value="' . __( 'Expand', 'privar' ) . '" data-alt="' . __( 'Contract', 'privar' ) . '" data-action="1" />';
					}
					$this->table .= "</div>";

					// If is paginated - Pagination
					$rpp = $this->get_arg( 'rows_per_page' );
					if ( $rpp != '' and intval($this->nrows) > intval($rpp) ) {
						$this->make_portion( 'pagination' );
					}

					$this->table .= '</div>';
				}

				break;
			}
			case 'post': {
				$this->table .= '<div class="tablenav bottom">';

				$this->table .= "<div class='alignleft actions'>";
				// If there are hideaways
				$hideaway = $this->get_arg( 'hideaway' );
				if ( !empty( $hideaway ) ) {
					$this->table .= '<input type="button" class="button action expand-button" value="' . __( 'Expand', 'privar' ) . '" data-alt="' . __( 'Contract', 'privar' ) . '" data-action="1" />';
				}
				$this->table .= "</div>";

				// If is paginated
				$rpp = $this->get_arg( 'rows_per_page' );
				if ( $rpp != '' and intval($this->nrows) > intval($rpp) ) {
					$this->make_portion( 'pagination' );
				}

				$this->table .= '</div>';
				break;
			}
			case 'head': {
				// Prepare <thead>
				$this->table .= '<thead><tr>';

				if ( $this->checkbox ) {
					$this->make_portion( 'head-checkbox' );
				}

				foreach ( $this->get_arg( 'columns' ) as $name => $label ) {
					if ( !in_array( $name, $this->get_arg( 'hidden' ) ) ) {

						if ( in_array( $name, $this->get_arg( 'sortable' ) ) ) {
							// If sortable
							$this->table .= "<th id='$name' class='manage-column column-$name";

							// If hideaway
							if ( in_array( $name, $this->get_arg( 'hideaway' ) ) ) {
								$this->table .= ' hideaway';
							}

							// If sorted
							if ( $this->sorted == true and $this->orderby == $name ) {
								$order = $this->order;
								$this->table .= " sorted $order";
							} else {
								$order = 'asc';
								$this->table .= " sortable $order";
							}
							$this->table .= "' scope='col'>";

							$this->table .= "<a href='" . $this->get_arg( 'base_uri' ) . "&orderby=$name&order=";
							if ( $order == 'asc' ) { $this->table .= "desc"; }
							else { $this->table .= 'asc'; }
							$this->table .= "'><span>$label</span><span class='sorting-indicator'></span></a></th>";
						} else {
							// If not sortable
							$this->table .= "<th id='$name' class='manage-column column-$name";

							// If hideaway
							if ( in_array( $name, $this->get_arg( 'hideaway' ) ) ) {
								$this->table .= ' hideaway';
							}

							$this->table .= "' scope='col'>$label</th>";
						}

					}
				}

				$this->table .= '</tr></thead>';

				break;
			}
			case 'foot': {
				// Prepare <tfoot>
				$this->table .= '<tfoot><tr>';

				if ( $this->checkbox ) {
					$this->make_portion( 'foot-checkbox' );
				}

				foreach ( $this->get_arg( 'columns' ) as $name => $label ) {
					if ( !in_array( $name, $this->get_arg( 'hidden' ) ) ) {

						if ( in_array( $name, $this->get_arg( 'sortable' ) ) ) {
							// If sortable
							$this->table .= "<th id='$name' class='manage-column column-$name";

							// If hideaway
							if ( in_array( $name, $this->get_arg( 'hideaway' ) ) ) {
								$this->table .= ' hideaway';
							}

							// If sorted
							if ( $this->sorted == true and $this->orderby == $name ) {
								$order = $this->order;
								$this->table .= " sorted $order";
							} else {
								$order = 'asc';
								$this->table .= " sortable $order";
							}
							$this->table .= "' scope='col'>";

							$this->table .= "<a href='" . $this->get_arg( 'base_uri' ) . "&orderby=$name&order=";
							if ( $order == 'asc' ) { $this->table .= "desc"; }
							else { $this->table .= 'asc'; }
							$this->table .= "'><span>$label</span><span class='sorting-indicator'></span></a></th>";
						} else {
							// If not sortable
							$this->table .= "<th id='$name' class='manage-column column-$name";

							// If hideaway
							if ( in_array( $name, $this->get_arg( 'hideaway' ) ) ) {
								$this->table .= ' hideaway';
							}

							$this->table .= "' scope='col'>$label</th>";
						}

					}
				}

				$this->table .= '</tr></tfoot>';

				break;
			}
			case 'head-checkbox': {
				$this->table .= "<th id='cb' class='manage-column column-cb check-column' scope='col'>";
				$this->table .= "<label class='screen-reader-text' for='cb-select-all-1'>" . __( 'Seleziona tutto', 'privar' ) . "</label>";
				$this->table .= "<input id='cb-select-all-1' type='checkbox' />";
				$this->table .= "</th>";

				break;
			}
			case 'foot-checkbox': {
				$this->table .= "<th id='cb' class='manage-column column-cb check-column' scope='col'>";
				$this->table .= "<label class='screen-reader-text' for='cb-select-all-2'>" . __( 'Seleziona tutto', 'privar' ) . "</label>";
				$this->table .= "<input id='cb-select-all-2' type='checkbox' />";
				$this->table .= "</th>";

				break;
			}
			case 'pagination': {
				if ( $this->paged ) { $currpage = $this->page; }
				else { $currpage = 1; }
				$residue = $this->nrows % $this->get_arg( 'rows_per_page' );
				$npages = ($this->nrows - $residue ) / $this->get_arg( 'rows_per_page' );
				if ( $residue != 0 ) { $npages++; }

				$this->table .= "<div class='tablenav-pages'>";
				$this->table .= "<span class='displaying-num'>" . $this->nrows . " " . __( 'elementi', 'privar' ) . "</span>";
				$this->table .= "<span class='pagination-links'>";

				$this->table .= "<a class='first-page";
				if ( $currpage == 1 ) { $this->table .= " disabled'"; }
				else { $this->table .= "' href='" . $this->get_arg( 'base_uri' ) . "&edit_post_per_page=" . $this->perpage . "'"; }
				$this->table .= ">&laquo;</a>";

				$this->table .= "<a class='prev-page";
				if ( $currpage == 1 ) { $this->table .= " disabled'"; }
				else { $this->table .= "' href='" . $this->get_arg( 'base_uri' ) . "&paged=" . ($currpage-1) . "&edit_post_per_page=" . $this->perpage . "'"; }
				$this->table .= ">&lsaquo;</a>";

				$this->table .= "<span class='paging-input'>";
				$this->table .= '<input class="current-page" type="text" size="1" value="' . $currpage . '" name="paged" data-target="' . $this->get_arg( 'base_uri' ) . '"></input>';
				$this->table .= " di ";
				$this->table .= "<span class='total-pages'>" . $npages . "</span>";
				$this->table .= "</span>";

				$this->table .= "<a class='next-page";
				if ( $currpage == $npages ) { $this->table .= " disabled'"; }
				else { $this->table .= "' href='" . $this->get_arg( 'base_uri' ) . "&paged=" . ($currpage+1) . "&edit_post_per_page=" . $this->perpage . "'"; }
				$this->table .= ">&rsaquo;</a>";

				$this->table .= "<a class='last-page";
				if ( $currpage == $npages ) { $this->table .= " disabled'"; }
				else { $this->table .= "' href='" . $this->get_arg( 'base_uri' ) . "&paged=" . $npages . "&edit_post_per_page=" . $this->perpage . "'"; }
				$this->table .= ">&raquo;</a>";

				$this->table .= "</span>";
				$this->table .= "</div>";

				break;
			}
			case 'page-options': {

				$this->table .= "<div id='screen-meta' class='metabox-prefs double'>";
				$this->table .= "<div id='screen-options-wrap' class='hidden' tabindex='-1'>";
				$this->table .= "<form id='adv-settings' method='post' action='" . $this->get_arg('base_uri') . "'>";
				$this->table .= "<div class='screen-options'>";
				$this->table .= "<input type='number' class='screen-per-page' value='" . $this->get_arg('rows_per_page') . "' maxlength='3' name='edit_post_per_page' id='edit_post_per_page' max='999' min='1' step='1' />";
				$this->table .= "&nbsp;<label for='edit_post_per_page'>" . __( 'Rows', 'privar' ) . "</label>";
				$this->table .= "<input type='submit' value='" . __( 'Save', 'privar' ) . "' class='button' />";
				$this->table .= "</div>";
				$this->table .= "</form>";
				$this->table .= "</div>";
				$this->table .= "</div>";
				$this->table .= "<div id='screen-meta-links' class='double'>";
				$this->table .= "<div id='screen-options-link-wrap' class='hide-if-no-js screen-meta-toggle'>";
				$this->table .= "<a id='show-settings-link' class='show-settings screen-meta-active' aria-expanded='false' aria-controls='screen-options-wrap' href='#screen-options-wrap'>";
				$this->table .= __( 'Page settings', 'privar' );
				$this->table .= "</a>";
				$this->table .= "</div>";
				$this->table .= "</div>";

				break;
			}
			case 'last': {
				$this->table .= '<script type="text/javascript">';
				$this->table .= 'jQuery(".hideaway").fadeOut(); jQuery(".expand-button").click(function(e) { e.preventDefault(); if ( jQuery(this).attr("data-action") == "0" ) { jQuery(".expand-button").attr("data-action","1"); alt=jQuery(this).attr("data-alt"); jQuery(".expand-button").attr("data-alt",jQuery(this).attr("value")).attr("value",alt); jQuery(".hideaway").fadeOut(); } else { jQuery(".expand-button").attr("data-action","0"); alt=jQuery(this).attr("data-alt"); jQuery(".expand-button").attr("data-alt",jQuery(this).attr("value")).attr("value",alt); jQuery(".hideaway").fadeIn(); } }); jQuery("input.current-page").each(function() { jQuery(this).change(function(e) { document.location.href = jQuery(this).attr("data-target") + "&paged=" + jQuery(this).attr("value"); }); }); jQuery("form#search-box").submit(function(e) { e.preventDefault(); document.location.href = jQuery(this).attr("data-target") + "&s=" + jQuery("input#post-search-input").attr("value"); }); jQuery(document).ready(function() { jQuery("#screen-meta-links.double").prependTo( "#wpbody-content" ); jQuery("#screen-meta.double").prependTo( "#wpbody-content" ); });';
				$this->table .= '</script>';

				break;
			}
		}
	}
	
	/**
	 * Retrieves an argument supplied to the __construct() function
	 * @param  String $name the requested argument name
	 * @return mix       returns the requested argument, correctly formatted
	 */
	public function get_arg( $name ) {
		switch ( $name ) {
			case 'checkbox': {
				if ( isset( $this->args[ $name ] ) ) {
					return $this->args[ $name ];
				} else {
					return true;
				}
				break;
			}

			case 'class': {
				if ( isset( $this->args[ $name ] ) ) {
					return ' ' . $this->args[ $name ];
				}
				break;
			}

			case 'columns': case 'base_uri': case 'table_name': {
				return $this->args[ $name ];
				break;
			}

			case 'hideaway': case 'hidden': case 'search_fields': case 'single_actions': case 'single_action_position': case 'sortable': {
				if ( isset( $this->args[ $name ] ) ) {
					return $this->args[ $name ];
				} else {
					return array();
				}
				break;
			}

			case 'id_column': {
				if ( isset( $this->args[ $name ] ) ) {
					return $this->args[ $name ];
				}
				break;
			}

			case 'is_table': case 'allow_edit_rows_per_page': {
				if ( isset( $this->args[ $name ] ) and $this->args[ $name ] === true ) {
					return true;
				} else {
					return false;
				}
			}

			case 'order': case 'orderby': {
				if ( isset( $this->args[ $name ] ) and is_string( @$this->args[ $name ] ) ) {
					return $this->args[ $name ];
				} else {
					return "";
				}
				break;
			}

			case 'rows_per_page': {
				if ( isset( $this->args[ $name ] ) and is_string( @$this->args[ $name ] ) ) {
					if ( $this->perpage != false ) {
						return $this->perpage;
					} else {
						return $this->args[ $name ];
					}
				} else {
					return "";
				}
				break;
			}

			case 'search_label': {
				if ( isset( $this->args[ $name ] ) and is_string( @$this->args[ $name ] ) ) {
					return $this->args[ $name ];
				} else {
					return __( 'Search', 'privar' );
				}
				break;
			}

			default: {
				return '';
			}
		}
	}
	
	/**
	 * Verifies that the argument array supplied to the __construct() function does not contain any error.
	 * @param  Array $arg the argument array supplied to __construct()
	 * @return Boolean      true if everything is ok
	 */
	public static function check_args( $arg ) {
		$response = true;

		// Check Columns
		if ( !isset( $arg['columns'] ) OR !is_array( @$arg['columns'] ) ) {
			$response = false;
		} else {
			foreach ( $arg['columns'] as $name => $label ) {
				if ( $name === '' OR $label === '') {
					$response = false;
				}
			}
		}

		// Check Sortable
		$labels = array ( 'sortable', 'hidden' );
		foreach ( $labels as $label ) {
			if ( isset( $arg[ $label ] ) ) {
				if ( !is_array( $arg[ $label ] ) ) {
					$response = false;
				} else {
					foreach ( $arg[ $label ] as $column ) {
						if ( $column === '' ) {
							$response = false;
						}
					}
				}
			}
		}

		// Check Multi_action/Single_action
		if ( isset( $arg['multi_actions'] ) ) {
			if ( !is_array( $arg['multi_actions'] ) ) {
				$response = false;
			} else {
				foreach ( $arg['multi_actions'] as $action => $uri ) {
					if ( $uri == '' ) {
						$response = false;
					}
				}
			}
		}

		if ( isset( $arg['single_actions'] ) ) {
			if ( !is_array( $arg['single_actions'] ) ) {
				$response = false;
			} else {
				foreach ( $arg['single_actions'] as $action => $opt ) {
					if ( count( $opt ) == 1 ) {
						if ( $opt[0] == '' ) {
							$response = false;
						}
					} elseif ( count( $opt ) == 2 ) {
						$c = $opt[1];
						if ( count( $c ) != 3 OR @$c[0] == '' OR !in_array( @$c[1], array( '==', '===', '!=', '<', '>', '<=','>=' ) ) OR @$c[2] == '' ) {
							$response = false;
						}
					} else { $response = false; }
				}
			}
		}

		// Check base_uri/table_name
		$labels = array( 'base_uri', 'table_name' );
		foreach ( $labels as $label ) {
			if ( !isset( $arg[ $label ] ) OR @$arg[ $label ] === '' ) {
				$response = false;
			}
		}

		// Check checkbox
		if ( isset( $arg['checkbox'] ) AND !is_bool( @$arg['checkbox'] ) ) {
			$response = false;
		}

		// Check ID_Column
		if ( isset( $arg['id_column'] ) AND @$arg['id_column'] == '' ) {
			$response = false;
		}

		return $response;
	}

}

?>

<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-load.php" );
require_once( "privar-settings.php" );
require_once( "lib/activator.class.php" );

$act = new Activator();

#If not activated automatically
if ( $act->automatic == false ) {
	$act->activate( $_GET['code'] );
}

#Go back home
header("refresh: 1; url='" . get_bloginfo( 'wpurl' ) . "'");

#Check activation
if ( $act->activated == true and $act->error == 0 ) {
	echo ACTMSG1;
}

#Errors
switch( $act->error ) {
	case 1: {
		echo ACTMSG2;
		break;
	}

	case 2: {
		echo ACTMSG3;
		break;
	}

	case 3: {
		echo ACTMSG4;
		break;
	}

	case 4: {
		echo ACTMSG5;
		break;
	}
}

?>
<?php
#Load wordpress built-in functions
require_once( dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-load.php" );
require_once( "privar-settings.php" );

#Retrieve plugin settings and build condition for access
switch( get_option( "privar-alsonly" ) ) {
	case 'n': { #no
		$condition = @$_SESSION[PASLOG] == PASLOGV;
		break;
	}

	case 'a': { #also
		$condition = @$_SESSION[PASLOG] == PASLOGV || is_user_logged_in();
		break;
	}

	case 'o': { #only
		$condition = is_user_logged_in();
		break;
	}

	case 'b': { #both
		$condition = @$_SESSION[PASLOG] == PASLOGV && is_user_logged_in();
		break;
	}

	default:
		$condition = @$_SESSION[PASLOG] == PASLOGV;
}

#If condition matches and a file is requested, return it
if ( isset( $_GET['id'] ) and $condition ) {
	$file = $_GET['id'];
	include( WP_CONTENT_DIR . "/privar-archive/download.inc.php" );
} else {
	echo PAMEDNM;
}

?>
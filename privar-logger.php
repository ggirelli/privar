<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-load.php" );
require_once( "privar-settings.php" );
require_once( "lib/logger.class.php" );

#Set $redirectto
if ( isset( $_SERVER['HTTP_REFERER'] ) and @$_SERVER['HTTP_REFERER'] != "" ) {
	$directto = $_SERVER['HTTP_REFERER'];
} else {
	$directto = get_bloginfo( 'wpurl' );
}

#Create new logger instance
$lgr = new Logger();

if ( isset( $_GET['logout'] ) ) {
	$_SESSION[PASLOG] = "";

	if ( get_option( 'privar-session-starter' ) == '1' ) { session_destroy(); }

	header("refresh: 1; url='" . $directto . "'");
	echo LOGMSG9;
	die();
}

#If already logged
if ( $lgr->isLogged ) {
	#Go back home
	header("refresh: 1; url='" . $directto . "'");
	echo LOGMSG1;
} else {
	#Control login form values
	if ( isset($_POST[PAUSRFIELD]) and isset($_POST[PAPWDFIELD]) and @$_POST[PAUSRFIELD] != "" and @$_POST[PAPWDFIELD] != "" ) {
		$usr = $_POST[PAUSRFIELD];
		$pwd = $_POST[PAPWDFIELD];

		if ( $lgr->isEmail( $usr ) ) {
			$usr = $lgr->getUserByEmail( $usr );
		}

		$go_on = false;
		# Captcha step?
		if ( !$lgr->isUser( $usr ) or $lgr->isBlocked( $usr ) ) {
			
			if ( isset( $_POST['captcha_code'] ) ) {
				$directto = $_POST['dt'];

				# Captcha response
				if ( $lgr->checkCaptcha( $_POST['captcha_code'] ) ) {
					$go_on = true;
				} else {
					# Go back home
					header("refresh: 1; url='" . $directto . "'");
					echo LOGMSG10;
				}
			} else {
				# Captcha form
				include( plugin_dir_path(__FILE__) . 'inc/unlock_captcha_form.inc.php' );
			}

		} else {
			$go_on = true;
		}

		if ( $go_on ) {
			#Check if given user exists
			if ( $lgr->isUser( $usr ) ) {

				#Check if given user is dropped
				if ( $lgr->isUserDropped( $usr ) ) {
					#Go back home
					header("refresh: 1; url='" . $directto . "'");
					echo LOGMSG4;
				} else {

					#Check if given user has database errors
					if ( $lgr->checkUser( $usr ) ) {

						#Check if given user was confirmed
						if ( $lgr->isUserConf( $usr ) ) {

							#Check if given pwd corresponds to given usr
							if ( $lgr->login( $usr, $pwd ) ) {
								#Go back home
								$lgr->emptyBlocker( $usr );
								header("refresh: 1; url='" . $directto . "'");
								echo LOGMSG8;
							} else {
								#Go back home
								$lgr->incrementBlocker( $usr );
								header("refresh: 1; url='" . $directto . "'");
								echo LOGMSG7;
							}

						} else {
							#Go back home
							$lgr->incrementBlocker( $usr );
							header("refresh: 1; url='" . $directto . "'");
							echo LOGMSG6;
						}

					} else {
						#Go back home
						header("refresh: 1; url='" . $directto . "'");
						echo LOGMSG5;
					}

				}

			} else {
				#Go back home
				$lgr->incrementBlocker( $usr );
				header("refresh: 1; url='" . $directto . "'");
				echo LOGMSG3;
			}
		}

	} else {
		#Go back home
		header("refresh: 1; url='" . $directto . "'");
		echo LOGMSG2;
	}
}
?>
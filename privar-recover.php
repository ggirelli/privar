<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-load.php" );
require_once( "privar-settings.php" );
require_once( "lib/recover.class.php" );

#Set $redirectto
if ( isset( $_SERVER['HTTP_REFERER'] ) and @$_SERVER['HTTP_REFERER'] != "" ) {
	$directto = $_SERVER['HTTP_REFERER'];
} else {
	$directto = get_bloginfo( 'wpurl' );
}

#Create new recover instance
$rec = new Recover();

# Second step: check code
if ( isset( $_GET['code'] ) and @$_GET['code'] != "" and $rec->is_code( @$_GET['code'] ) ) {

	# Already confirmed?
	if ( isset( $_POST['password'] ) and isset( $_POST['password2'] ) ) {

		$rec->endRecovery( $_GET['code'], $_POST[PAUSRFIELD], $_POST['password'], $_POST['password2'], $_POST['dt'] );

	} else {

		$code = $_GET['code'];

		_e( 'Fill the password recovery form:', 'privar' );

		include( 'inc/recover_confirm.form.inc.php' );

	}

	die();

}

# First step
if ( isset($_POST[PAUSRFIELD]) and @$_POST[PAUSRFIELD] != "" ) {
	$usr = $_POST[PAUSRFIELD];

	if ( $rec->isEmail( $usr ) ) {
		$usr = $rec->getUserByEmail( $usr );
	}

	#Check if given user exists
	if ( $rec->isUser( $usr ) ) {

		#Check if given user is dropped
		if ( $rec->isUserDropped( $usr ) ) {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo RECMSG3;
		} else {

			#Check if given user has database errors
			if ( $rec->checkUser( $usr ) ) {

				#Check if given user was confirmed
				if ( $rec->isUserConf( $usr ) ) {

					# Change pwd
					$rec->startRecovery( $usr, $directto );

				} else {
					#Go back home
					header("refresh: 1; url='" . $directto . "'");
					echo RECMSG5;
				}

			} else {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				echo RECMSG4;
			}
		}

	} else {
		#Go back home
		header("refresh: 1; url='" . $directto . "'");
		echo RECMSG2;
	}

} else {
	#Go back home
	header("refresh: 1; url='" . $directto . "'");
	echo RECMSG1;
}
?>
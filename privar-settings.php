<?php

/*----------------------*
 *		OPTIONS 		*
 *----------------------*/

# Set email options
add_option( 'privar-sign-email-admin-address', get_bloginfo( 'admin_email' ) );
add_option( 'privar-sign-email-sender-address', "noreply@" . get_bloginfo('name') . ".com" );
add_option( 'privar-sign-email-sender-name', 'Priv.Area Registration' );

add_option( 'privar-smtp-check', 0 );
add_option( 'privar-smtp-server', '' );
add_option( 'privar-smtp-port', '' );
add_option( 'privar-smtp-conn', 'no' );
add_option( 'privar-smtp-username', '' );
add_option( 'privar-smtp-password', '' );

# Captcha for blocked Users
add_option( 'privar-blocked-login', array() );
add_option( 'privar-login-attempt', 3 );

# Recover password
add_option( 'privar-recover-pwd', array() );
add_option( 'privar-recover-time', 1440);

# Private media
add_option( "privar-alsonly", "a");

# Session
add_option( 'privar-session-starter', '0' );
if ( session_id() == "" ) {
    session_start();
    update_option( 'privar-session-starter', '1' );
}

/*----------------------*
 *		CONSTANTS 		*
 *----------------------*/

#Email variable
define( 'EMAILKEYWORDLINK', 'PRIVARINDIRIZZO' );
define( 'EMAILKEYWORDTEXT', 'PRIVARMESSAGGIO' );
define( 'EMAILTEXT', __( 'To confirm your registration, open the link below or copy it in your browser.', 'privar' ) );
define( 'EMAILTEXT2', __( 'Your account has been activated by an administrator, now you can access the restricted area using username/email and password that you provided during the registration.', 'privar' ) );
define( 'RECEMAILTEXT', __( 'To change your password open the link below or copy it in your browser.<br />If you did not requested the password recovery, ignore this email.<br />The token will be de-activated after 24 hours.', 'privar' ) );
define( 'RECEMAILTEXT2', __( 'Your new password to access the restricted area of ' . get_bloginfo( 'name' ), 'privar' ));
define( 'EMAILSUBJECT', __( 'Confirm your registration to the restricted area of ' . get_bloginfo( 'name' ), 'privar' ));
define( 'EMAILSUBJECT2', __( 'Confirm the activation of the user to access the restricted area of ' . get_bloginfo( 'name' ), 'privar' ));
define( 'RECEMAILSUBJECT', __( 'Change the password to access the private area of ' . get_bloginfo( 'name' ), 'privar' ));
define( 'ADMEMAILSUBJECT', 'Warning: new user registration' );
define( 'ADMEMAILKEYWORDTEXT', 'PRIVARMESSAGGIO' );
define( 'ADMEMAILKEYWORDTEXT2', 'PRIVARDATI' );
define( 'ADMEMAILTEXT', __( 'Warning: a new user successfully signed up to the restricted area of ' . get_bloginfo('name'), 'privar' ) );
define( 'ADMEMAILSUBJECT2', __( 'IMPORTANT: SEVERE ERROR DURING USER ACTIVATION', 'privar' ) );
define( 'ADMEMAILTEXT2', __( 'Warning: during the activation of a new user for the restricted area of ' . get_bloginfo('name') . ' a severe error occured. Take a look ASAP!', 'privar' ) );

define( 'SMTPOPT', __( "Check to set a SMTP server to send emails.", 'privar' ) );
define( 'SMTPOPT1', __( "Server SMTP", 'privar' ) );
define( 'SMTPOPT2', __( "Porta SMTP", 'privar' ) );
define( 'SMTPOPT3', __( "Username", 'privar' ) );
define( 'SMTPOPT4', __( "Password", 'privar' ) );
define( 'SMTPTXT', __( 'Leave blank if no authentication is required.', 'privar' ) );
define( 'SMTPOPT5a', __( 'Safe connection', 'privar' ) );
define( 'SMTPOPT5b', __( 'No', 'privar' ) );
define( 'SMTPOPT5c', __( 'SSL', 'privar' ) );
define( 'SMTPOPT5d', __( 'TLS', 'privar' ) );

#Session variables
define( 'PASLOG', 'pa.user.auth' );
define( 'PASLOGV', 'true' );

#Names of login form fields
define( 'PAUSRFIELD', 'pa-usr');
define( 'PAPWDFIELD', 'pa-pwd');

#Logger messages
define( 'LOGMSG1', __( 'You are already logged!', 'privar'));
define( 'LOGMSG2', __( 'Logging in...<br/>Error 001: please fill the form correctly.', 'privar')); #Inviato form vuoto
define( 'LOGMSG3', __( 'Logging in...<br/>Error 002: wrong username or password.', 'privar')); #Utente inesistente
define( 'LOGMSG4', __( 'Logging in...<br/>Error 003: provided user no longer in the system.', 'privar')); #Utente cancellato
define( 'LOGMSG5', __( 'Logging in...<br/>Error 004: wrong user, please contact the administrator.', 'privar')); #Utente contenente errori nel database
define( 'LOGMSG6', __( 'Logging in...<br/>Error 005: user waiting for activation.', 'privar')); #Utente non confermato
define( 'LOGMSG7', __( 'Logging in...<br/>Error 006: wrong username or password.', 'privar')); #Password errata
define( 'LOGMSG8', __( 'Logging in...<br/>Correctly logged in.', 'privar'));
define( 'LOGMSG9', __( 'Logging out...<br/>Correclty logged out.', 'privar'));
define( 'LOGMSG10', __( 'Logging in...<br/>Error 007: try again.', 'privar')); #Captcha failed

#Recovery messages
define( 'RECMSG1', __( 'Recovering...<br/>Error 001: please fill the form correctly.', 'privar')); #Inviato form vuoto
define( 'RECMSG2', __( 'Recovering...<br/>Error 002: wrong username.', 'privar')); #Utente inesistente
define( 'RECMSG3', __( 'Recovering...<br/>Error 003: provided user no longer in the system.', 'privar')); #Utente cancellato
define( 'RECMSG4', __( 'Recovering...<br/>Error 004: wrong user, please contact the administrator.', 'privar')); #Utente contenente errori nel database
define( 'RECMSG5', __( 'Recovering...<br/>Error 005: user waiting for activation.', 'privar')); #Utente non confermato

#Signer messages
define( 'SIGMSG1', __( 'Signing up...<br/>Error 001: try again.', 'privar') ); #Captcha errato
define( 'SIGMSG2', __( 'Signing up...<br/>Error 002: please fill the form correctly.', 'privar' ) ); #Campi vuoti o form errato
define( 'SIGMSG3', __( 'Signing up...<br/>Error 003: wrong country.', 'privar' ) ); #Stato indicato di lunghezza errata
define( 'SIGMSG4', __( 'Signing up...<br/>Error 004: wrong email.', 'privar' ) ); #Email errata
define( 'SIGMSG5', __( 'Signing up...<br/>Error 005: the supplied passwords do not coincide.', 'privar' ) ); #Password non coincidenti
define( 'SIGMSG6', __( 'Signing up...<br/>Error 006: username not available.', 'privar' ) ); #Password non coincidenti
define( 'SIGMSG7', __( 'Signing up...<br/>Error 007: zipcode, telephone, telefax or mobile wrong.', 'privar' ) ); #Uno dei campi numeri è errato
define( 'SIGMSG8', __( 'Signing up...<br/>Error 008: a user with the supplied email is already present in the system.', 'privar' ) ); #Email già utilizzata
define( 'SIGMSG9', __( 'Signing up...<br/>Correctly signed up, please follow the instruction that have been sent to your email.', 'privar' ) ); #
define( 'SIGMSG10', __( 'Signing up...<br/>Error 009: failed to sign up, try again later.', 'privar' ) ); #
define( 'SIGMSG11', __( 'Signing up...<br/>Error 010: sign up has been blocked, please contact the system administrator.', 'privar' ) ); #

#Sign in form texts
define( 'CAPTCHATEXT', __( 'Change image', 'privar' ) );

#Activation messages
define( 'ACTMSG1', __( 'Activating...<br/>User correctly activated.', 'privar' ) );
define( 'ACTMSG2', __( 'Activating...<br/>Error 001: wrong code.', 'privar' ) ); #Codice inesistente
define( 'ACTMSG3', __( 'Activating...<br/>Error 002: code already used.', 'privar' ) ); #Utente già attivo
define( 'ACTMSG4', __( 'Activating...<br/>Error 003: failed activation, try again later.', 'privar' ) ); #Problema nell'esecuzione della query
define( 'ACTMSG5', __( 'Activating...<br/>Error 004: severe error, please contact the system administrator.', 'privar' ) ); #Più utenti sono stati attivati da un solo codice

#Private media
define( 'PASET0', __( 'Settings', 'privar' ) );
define( 'PASET0b', __( 'Users', 'privar' ) );
define( 'PASET0c', __( 'Archive', 'privar' ) );
define( 'PASET1', __( 'Settings updated.', 'privar' ) );
define( 'PASET2', __( 'In 5 sec you will be redirected to the settings page.', 'privar' ) );
define( 'PASET3', __( 'Save', 'privar' ) );
define( 'PASET3b', __( 'Test email', 'privar' ) );
define( 'PASETFE1', __( 'Select the condition of access to the restricted area:', 'privar' ) );
define( 'PASETFE2', __( 'Allow access <b>also</b> to the users logged in <b>WordPress</b>.', 'privar' ) );
define( 'PASETFE3', __( 'Allow access <b>only</b> to the users logged in <b>WordPress</b>.', 'privar' ) );
define( 'PASETFE4', __( 'Allows access to the users logged in both <b>WordPress</b> and <b>Privar</b>.', 'privar' ) );
define( 'PASETFE5', __( 'Allow access <b>only</b> to the users logged in <b>Privar</b>.', 'privar' ) );
define( 'PASETFE6', __( 'You can set name and address of the mail-bot that will send warnings and requestes to the users.', 'privar' ) );
define( 'PASETFE7', __( 'Bot address: ', 'privar' ) );
define( 'PASETFE7b', __( "Restricted area administrator's address: ", 'privar' ) );
define( 'PASETFE8', __( 'Bot name: ', 'privar' ) );
define( 'PAMEDNM', __( 'You are not authorized to access this page.', 'privar' ) );
define( 'PAMEDNM2', __( 'You are not authorized to access this content.', 'privar' ) );
define( 'PAMEDDEL0', __( 'Do you really want to delete the file', 'privar' ) );
define( 'PAMEDDEL1', __( 'File removed.<br/>', 'privar' ) );
define( 'PAMEDDEL2', __( 'File not found.<br/>', 'privar' ) );
define( 'PAMEDDEL3', __( 'Yes', 'privar' ) );
define( 'PAMEDDEL4', __( 'No', 'privar' ) );
define( 'PAMED1', __( 'In 5 sec you will be redirected to the archive page.', 'privar' ) );
define( 'PAMEDTIT0', __( 'Restricted Archive Manager', 'privar' ) );
define( 'PAMEDTIT1', __( 'Restricted Archive', 'privar' ) );
define( 'PAMEDUP1', __( 'Error', 'privar' ) );
define( 'PAMEDUP2', __( 'File uploaded as:', 'privar' ) );
define( 'PAMEDUP3', __( 'Remember that you can always upload files by FTP to the directory \'wp-content/privar-archive/\'.<br />Select the file to be uploaded in the restricted archive:', 'privar' ) );
define( 'PAMEDUP4', __( 'Upload', 'privar' ) );
define( 'PAARCH1', __( 'Name', 'privar' ) );
define( 'PAARCH2', __( 'Link', 'privar' ) );
define( 'PAARCH3', __( 'Options', 'privar' ) );
define( 'PAARCHOPT1', __( 'Remove', 'privar' ) );
define( 'PAARCHNF', __( 'There are no Restricted Media.', 'privar' ) );
define( 'PAUSACT0', __( 'Updating the user...', 'privar' ) );
define( 'PAUSACT1', __( 'Updated.', 'privar' ) );
define( 'PAUSACT2', __( 'Update failed.', 'privar' ) );
define( 'PAUSER0', __( 'No user found', 'privar' ) );
define( 'PAUSERFS0', __( 'Waiting', 'privar' ) );
define( 'PAUSERFS1', __( 'Inactive', 'privar' ) );
define( 'PAUSERFS2', __( 'Active', 'privar' ) );
define( 'PAUSERFD0', __( 'No', 'privar' ) );
define( 'PAUSERFD1', __( 'Yes', 'privar' ) );
define( 'PAUSEROPT0', __( 'Activate', 'privar' ) );
define( 'PAUSEROPT1', __( 'De-activate', 'privar' ) );
define( 'PAUSEROPT2', __( 'Remove', 'privar' ) );
define( 'PAUSEROPT3', __( 'Recover', 'privar' ) );
define( 'PAUSERT1', __( 'Username', 'privar' ) );
define( 'PAUSERT2', __( 'Name', 'privar' ) );
define( 'PAUSERT3', __( 'Last name', 'privar' ) );
define( 'PAUSERT4', __( 'Status', 'privar' ) );
define( 'PAUSERT5', __( 'Removed', 'privar' ) );
define( 'PAUSERT6', __( 'Options', 'privar' ) );

#Forms
define( 'PASEND', __( 'Send', 'privar' ) );
define( 'PASIGN', __( 'Sign up', 'privar' ) );

?>
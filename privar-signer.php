<?php
#Load required functions
require_once( dirname(dirname(dirname(dirname(__FILE__)))) . "/wp-load.php" );
require_once( "privar-settings.php" );
require_once( "lib/signer.class.php" );

$signer = new Signer();

#Set $redirectto
if ( isset( $_SERVER['HTTP_REFERER'] ) and @$_SERVER['HTTP_REFERER'] != "" ) {
	$directto = $_SERVER['HTTP_REFERER'];
} else {
	$directto = get_bloginfo( 'wpurl' );
}

#Check Captcha
if ( $signer->checkCaptcha() ) {

	#Check FormData
	switch ( $signer->checkFormData() ) {
		case 0: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG2;
			break;
		}

		case 1: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG3;
			break;
		}

		case 2: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG4;
			break;
		}

		case 3: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG5;
			break;
		}

		case 4: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG6;
			break;
		}

		case 5: case 6: case 7: case 8: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG7;
			break;
		}

		case 9: {
			#Go back home
			header("refresh: 1; url='" . $directto . "'");
			echo SIGMSG8;
			break;
		}

		case 10: {

			$ris = $signer->signin();

			if ( $ris == true ) {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				echo SIGMSG9;
			} elseif ( $ris == false ) {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				echo SIGMSG10;
			} else {
				#Go back home
				header("refresh: 1; url='" . $directto . "'");
				echo SIGMSG11 . "\n" . $ris;
			}
			
			break;
		}
	}
	
} else {
	#Go back home
	header("refresh: 1; url='" . $directto . "'");
	echo SIGMSG1;
}

?>
<?php
/**
 * Plugin Name: Private Area
 * Description: Allows to create a private area and separate login platform with private media and private posts
 * Version: 4.6.1
 * Author: ggirelli
 * Author URI: http://filopoe.it
 * Text Domain: privar
 * License: GPL2
 * Included software: PHPMailer (GNU2.1), SecurImage (MIT)
 */

/*  Copyright 2013  Gabriele Girelli  < gabriele@filopoe.it >

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

require_once( "privar-settings.php" );

/* Load text domain */
function loadTextDomain() {
    load_plugin_textdomain(
        'privar',
        false,
        dirname( plugin_basename( __FILE__ ) ) . '/lang'
    ); 
}
add_action( 'plugins_loaded', 'loadTextDomain' );

/*----------------------------------*
 *            ADMIN MENU            *
 *----------------------------------*/

add_action( 'admin_menu', 'add_privar_menu' );

function add_privar_menu(){
    add_menu_page( 'Private Area - ' . PASET0, 'Private Area', 'manage_options', 'privar', 'privar_menu_content_settings', 'dashicons-lock', 69 );
    add_submenu_page( 'privar', 'Private Area - ' . PASET0, 'PA ' . PASET0, 'manage_options', 'privar', 'privar_menu_content_settings' );
    add_submenu_page( 'privar', 'Private Area - ' . PASET0b, 'PA ' . PASET0b, 'edit_published_pages', 'privar-users', 'privar_menu_content_users' );
    add_submenu_page( 'privar', 'Private Area - ' . PASET0c, 'PA ' . PASET0c, 'edit_published_pages', 'privar-media', 'privar_menu_content_media' );
}
function privar_menu_content_settings(){ include("admin/settings_page.php"); }
function privar_menu_content_users(){ include("admin/users_page.php"); }
function privar_menu_content_media(){ include("admin/media_page.php"); }

/*----------------------------------*
 *             DATABASE             *
 *----------------------------------*/

require_once( "lib/padb.class.php" );
$padb = new PADB();


/*----------------------------------*
 *             WIDGET               *
 *----------------------------------*/

# Register and load login/logout widget
require_once( "widget/logio_widget.class.php" );
function privar_load_login_widget() {
    register_widget( 'Privar_logio_widget' );
}
add_action( 'widgets_init', 'privar_load_login_widget' );

# Register and load signin widget
require_once( "widget/signin_widget.class.php" );
function privar_load_signin_widget() {
    register_widget( 'Privar_signin_widget' );
}
add_action( 'widgets_init', 'privar_load_signin_widget' );

# Register and load content widget
require_once( "widget/content_widget.class.php" );
function privar_load_content_widget() {
    register_widget( 'Privar_content_widget' );
}
add_action( 'widgets_init', 'privar_load_content_widget' );

# Register and load login/content widget
require_once( "widget/logio_content_widget.class.php" );
function privar_load_logio_content_widget() {
    register_widget( 'Privar_logio_content_widget' );
}
add_action( 'widgets_init', 'privar_load_logio_content_widget' );

# Register and load password-recovery widget
require_once( "widget/rec_pwd_widget.class.php" );
function privar_load_rec_pwd_widget() {
    register_widget( 'Privar_rec_pwd_widget' );
}
add_action( 'widgets_init', 'privar_load_rec_pwd_widget' );


/*--------------------------------------*
 *             SHORTCODES               *
 *--------------------------------------*/
require_once( "lib/logger.class.php" );

//[privar-logio]
function privar_logio_shortcode( $atts ) {
    extract( shortcode_atts( array(
            'title_out' => __( 'Logout from restricted area', 'privar' ),
            'title_in' => __( 'Sign in to restricted area', 'privar' ),
        ), $atts, 'privar-logio' ) );

    $lgr = new Logger();

    if ( $lgr->isLogged ) {
        echo "<h3>" . $title_out . "</h3>";
        include( "inc/logout_form.php" );
        echo "<br /><br />";
    } else {
        echo "<h3>" . $title_in . "</h3>";
        include( "inc/login_form.php" );
        echo "<br /><br />";
    }
}
add_shortcode( 'privar-logio', 'privar_logio_shortcode' );

//[privar-signin]
function privar_signin_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'title' => __( 'Sign up to access the restricted area.', '')
    ), $atts, 'privar-signin' ) );

    echo "<h3>" . $title . "</h3>";
    include( "inc/signin_form.php" );
    echo "<br /><br />";
}
add_shortcode( 'privar-signin', 'privar_signin_shortcode' );

//[privar]
function privar_shortcode( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'title' => __( 'Restricted area', 'privar'),
        'alt' => PAMEDNM2,
    ), $atts, 'privar' ) );

    $lgr = new Logger();

    echo "<h3>" . $title . "</h3>";
    if ( $lgr->isLogged ) {
        echo $content;
    } else {
        echo $alt;
    }
    echo "<br /><br />";
}
add_shortcode( 'privar', 'privar_shortcode' );

//[privar-recover]
function privar_rec_pwd_shortcode( $atts ) {
    extract( shortcode_atts( array( 'title' => __( 'Recover password', 'privar' ) ), $atts, 'privar-recover' ) );

    echo "<h3>" . $title . "</h3>";
    include( "inc/recover_request.form.inc.php" );
    echo "<br /><br />";
}
add_shortcode( 'privar-recover', 'privar_rec_pwd_shortcode' );


/*-----------------------------------*
 *             ARCHIVE               *
 *-----------------------------------*/



$dirname = 'privar-archive';
$filename = WP_CONTENT_DIR . '/' . $dirname . "/";

if (!file_exists($filename)) {
    mkdir( WP_CONTENT_DIR . '/' . $dirname, 0777 );

    $src = dirname(__FILE__) . '/archive/';
    $dst = WP_CONTENT_DIR . '/' . $dirname . '/';
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 

}



?>
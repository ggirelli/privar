=== Privar ===
Contributors: ggirelli
Tags: private, area, restricted, user, file
Requires at least: 3.8.1
Tested up to: 3.8.1
Stable tag: 4.3
Version: 4.6.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Privar creates a private area to upload protected files, accessible only to users registered to the area and previously approved.

== Description ==

Privar creates a protected directory in which you can upload any file. The address (URL) of the protected files can be shared to anyone, but will be accessible only to those users that you approved.

Users can:

1. register to the private area independently
2. register to your Wordpress website and access the files
3. both

Users can register to the private area as follows:

1. Compile di Sign-in form
2. Admin will recieve an email indicating that a new user wants to register.
The new user will recieve an email with a link to pre-activate its account.
3. Once the new user account is pre-activated, the admin MUST activate it manually from the Users page of the plugin.
4. That's all!

Also, Privar allows you to show content in the sidebars or in any page/post using its shortcodes and widgets!

Shortcodes:

* [privar]: hidden content
* [privar-logio]: login/out form
* [privar-signin]: sign-up form
* [privar-recover]: password recovery form

== Installation ==

1. Upload `privar.zip` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

*none*

== Screenshots ==

*none*

== Changelog ==

=  4.6.1 =
* More info on activation-request email

= 4.6.0 =
* Default order of users' table based on registration date
* Storage of last_login data

= 4.5.3 =
* Granted to user the ability to change the number of rows per page, in paginated tables
* Fixed minor bugs

= 4.5.0 =
* Password recovery
* Captcha after 'n' failed login attempts
* Translations

== Upgrade Notice ==

= 4.6.0 =
New features.

= 4.5.0 =
Many new features, also safer.

<?php

class Privar_content_widget extends WP_Widget {

	// constructor
	function __construct() {
		parent::__construct( 'privar_content', __( 'Privar Widget - Content', 'privar' ),
			array( 'description' => __( 'Content available if logged into Privar.', 'privar' ), ) );
	}

	// widget display
	function widget($args, $instance) {
		require_once( dirname(dirname(__FILE__)) . "/lib/logger.class.php" );

		#Create new logger instance
		$lgr = new Logger();

		#Retrieve plugin settings and build condition for access
		switch( get_option( "privar-alsonly" ) ) {
			case 'n': { #no
				$condition = @$_SESSION[PASLOG] == PASLOGV;
				break;
			}

			case 'a': { #also
				$condition = @$_SESSION[PASLOG] == PASLOGV || is_user_logged_in();
				break;
			}

			case 'o': { #only
				$condition = is_user_logged_in();
				break;
			}

			case 'b': { #both
				$condition = @$_SESSION[PASLOG] == PASLOGV && is_user_logged_in();
				break;
			}

			default:
				$condition = @$_SESSION[PASLOG] == PASLOGV;
		}

		#If condition matches show content
		if ( $condition ) {
			$title = apply_filters( 'widget_title', $instance['title'] );
			$content = apply_filters( 'widget_content', $instance['content'] );

			// before and after widget arguments are defined by themes
			echo $args['before_widget'];
			if ( ! empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];
			
			echo $content;

			echo $args['after_widget'];
		}
	}

	// widget form creation
	function form($instance) {	

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Private Area Content', 'privar' );
		}

		if ( isset( $instance['content'] ) ) {
			$content = $instance['content'];
		} else {
			$content = "";
		}

		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php _e( 'Content:' ); ?></label>
		<textarea class="widefat" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>" type="text"><?php echo esc_attr( $content ); ?></textarea>
		</p>
		<?php 

	}

	// widget update
	function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['content'] = ( ! empty( $new_instance['content'] ) ) ? $new_instance['content'] : '';
		return $instance;
	}

}

?>
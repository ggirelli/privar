<?php

class Privar_logio_content_widget extends WP_Widget {

	// constructor
	function __construct() {
		parent::__construct( 'privar_login_content', __( 'Privar Widget - Login with Content', 'privar' ),
			array( 'description' => __( 'Privar log in form, content showed if logged in.', 'privar' ), ) );
	}

	// widget display
	function widget($args, $instance) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$content = apply_filters( 'widget_content', $instance['content'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		require_once( dirname(dirname(__FILE__)) . "/lib/logger.class.php" );

		#Create new logger instance
		$lgr = new Logger();

		#Retrieve plugin settings and build condition for access
		switch( get_option( "privar-alsonly" ) ) {

			case 'a': { #also
				if ( $lgr->isLogged || is_user_logged_in() ) {
					echo $content . "<br /><br />";
					if ( $lgr->isLogged ) { include( dirname(dirname(__FILE__)) . "/inc/logout_form.php" ); }
				} else {
					include( dirname(dirname(__FILE__)) . "/inc/login_form.php" );
				}
				break;
			}

			case 'o': { #only
				if ( is_user_logged_in() ) {
					echo $content;
				} else {
					echo PAMEDNM2;
				}
				break;
			}

			case 'b': { #both
				if ( $lgr->isLogged && is_user_logged_in() ) {
					echo $content . "<br /><br />";
					include( dirname(dirname(__FILE__)) . "/inc/logout_form.php" );
				} else {
					if ( $lgr->isLogged ) {
						echo PAMEDNM2;
						include( dirname(dirname(__FILE__)) . "/inc/logout_form.php" );
					} else {
						include( dirname(dirname(__FILE__)) . "/inc/login_form.php" );
					}
				}
				break;
			}

			default: #'n' #none
				if ( $lgr->isLogged ) {
					echo $content . "<br /><br />";
					include( dirname(dirname(__FILE__)) . "/inc/logout_form.php" );
				} else {
					include( dirname(dirname(__FILE__)) . "/inc/login_form.php" );
				}
		}
		
		echo $args['after_widget'];
	}

	// widget form creation
	function form($instance) {	

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Private Area', 'privar' );
		}

		if ( isset( $instance['content'] ) ) {
			$content = $instance['content'];
		} else {
			$content = "";
		}

		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		<label for="<?php echo $this->get_field_id( 'content' ); ?>"><?php _e( 'Content:' ); ?></label>
		<textarea class="widefat" id="<?php echo $this->get_field_id( 'content' ); ?>" name="<?php echo $this->get_field_name( 'content' ); ?>" type="text"><?php echo esc_attr( $content ); ?></textarea>
		</p>
		<?php 

	}

	// widget update
	function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['content'] = ( ! empty( $new_instance['content'] ) ) ? $new_instance['content'] : '';
		return $instance;
	}

}

?>
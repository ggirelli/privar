<?php

class Privar_rec_pwd_widget extends WP_Widget {

	// constructor
	function __construct() {
		parent::__construct( 'privar_rec_pwd', __( 'Privar Widget - Recovery Password', 'privar' ),
			array( 'description' => __( 'Privar recovery password form.', 'privar' ), ) );
	}

	// widget display
	function widget($args, $instance) {
		$title = apply_filters( 'widget_title', $instance['title'] );

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		include( dirname(dirname(__FILE__)) . "/inc/recover_request.form.inc.php" );
		
		echo $args['after_widget'];
	}

	// widget form creation
	function form($instance) {	

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Private Area Recovery Password', 'wpb_widget_domain' );
		}

		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<?php 

	}

	// widget update
	function update($new_instance, $old_instance) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

?>